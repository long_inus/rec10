MAJOR = 0
MINOR = 9
REVISION = 10
VER = $(MAJOR).$(MINOR).$(REVISION)


all: 
	cd rec10;$(MAKE) all
	cd tstools;$(MAKE) all
	cd rectool;Makefile.PL
	cd tunerec;$(MAKE) all
clean:
	cd tstools;$(MAKE) clean
install: 
	cd rec10;$(MAKE) install
	cd tstools;$(MAKE) install
	cd tunerec;$(MAKE) install
uninstall:
	cd tstools;$(MAKE) uninstall
	cd rec10;$(MAKE) uninstall
	cd tunerec;$(MAKE) uninstall
	
