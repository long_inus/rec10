/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2012 Yukikaze
 */

package jtssplitter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yukikaze
 */
public class Chlist {
    public void writeCHList(String tspath,String listpath,int min,int max){
        FileWriter fw = null;
        try {
            Tsfile tsf = new Tsfile();
            Integer[] programnum = tsf.getProgramNum_byte(tspath);
            String str = "";
            for (int i = 0; i < programnum.length; i++) {
                if (!(min >0 && min>programnum[i])){
                    if (!(max >0 && max<programnum[i]))
                        str = str + programnum[i].toString() + "\n";
                }
            }
            fw = new FileWriter(listpath);
            fw.write(str);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(Chlist.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(Chlist.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
