/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;

/**
 * 記述子の内容を保持
 * @author yukikaze
 */
public class DescriptorData{
    public DescriptorData(){

    }
    public DescriptorData(int descriptor_tag,int value){
        Descriptor_tag=descriptor_tag;
        Value=value;
    }
    /**
     * 記述子タグ
     * 0x08:動画タグ::
     * -value::
     * -0:1080p
     * -1:1080i
     * -2:720p
     * -3:480p
     * -4:480i
     * -5:240p
     * -6:120p
     * -7:2160p
     * 0xC4:音声タグ::
     * -value::
     * -1:1/0(シングルモノラル)
     * -2:1/0+1/0(デュアルモノラル)
     * -3:2/0
     * -4:2/1
     * -7:3/1
     * -8:3/2
     * -9:3/2+LFE (5.1chサラウンド)
     *
     */
    public int Descriptor_tag;
    public int Value;
}
