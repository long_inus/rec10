/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;
import java.util.ArrayList;
/**
 *
 * @author yukikaze
 */
public class EITData {
    public int program_number;
    public boolean current_newt_indicator;
    public int section_number;
    public int event_id;
    public ArrayList<jtssplitter.data.descriptor.abstract_Descriptor> descriptors;
}
