/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;

/**
 * PATの一番組についてのデータ
 * @author Yukikaze
 */
public class PATData {
    /**
     * PAT内のプログラム番号
     * BS/CS放送ではチャンネル名になっていることが多い
     */
    public int Program_TABLE;
    /**
     * プログラム番号に対応したPMTが含まれるPID
     */
    public int PID;
    boolean containVideo=false;
}
