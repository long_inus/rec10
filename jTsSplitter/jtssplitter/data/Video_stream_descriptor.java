/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data;
import jtssplitter.calc;
/**
 * ビデオ記述子
 * 定義はISO/IEC 13818-1:2000(E) Table 2-40による
 * @author yukikaze
 */

public class Video_stream_descriptor {
    private int descriptor_tag;//0-7
    private int descriptor_length;//8-15
    private boolean multiple_frame_rate_flag;//16
    private String frame_rate_code;//17-20
    /**
     * frame rate
     * 0001 : 24000/1001
     * 0010 : 24
     * 0011 : 25
     * 0100 : 30000/1001
     * 0101 : 30
     * 0110 : 50
     * 0111 : 60000/1001
     * 1000 : 60
     *
     **/
    private jtssplitter.calc cal=new jtssplitter.calc();
    public void analyze(String s){
        descriptor_tag=cal.TSString2Int(s,0,8);
        multiple_frame_rate_flag=cal.TSString2Int(s,16,1)==1;
        frame_rate_code=cal.Int2String(cal.TSString2Int(s, 17, 4),4);
        s="";
        //descriptor_tag=
    }

}
