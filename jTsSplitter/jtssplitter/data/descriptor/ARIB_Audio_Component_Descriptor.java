/*
 * jTsSplitter - java based mpeg2ts splitter.
 * Copyright (C) 2009-2010 Yukikaze
 */

package jtssplitter.data.descriptor;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yukikaze
 */
public class ARIB_Audio_Component_Descriptor extends jtssplitter.data.descriptor.abstract_Descriptor{
    private int Descriptor;
    private int compornent_type;
    private int compornent_tag;
    private int stream_type;
    private boolean ES_multi_lingual_flag;
    private boolean main_component_flag;
    private int quality_indicator;
    private int sampling_rate;
    private int ISO_639_language_code;
    private int ISO_639_language_code_2;
    private String text;
    @Override
    public int getDescriptorTag() {
        return Descriptor;
    }

    @Override
    public void analyzeDescriptor(byte[] descriptor) {
        Descriptor=descriptor[0]&0xFF;
        compornent_type=descriptor[3]&0xFF;
        compornent_tag=descriptor[4]&0xFF;
        stream_type=descriptor[5]&0xFF;
        ES_multi_lingual_flag=(((descriptor[7]&0x80)>>7)==1);
        main_component_flag=(((descriptor[7]&0x40)>>6)==1);
        quality_indicator=(descriptor[7]&0x30>>4);
        sampling_rate=(descriptor[7]&0xE>>1);
        ISO_639_language_code=(((descriptor[8]&0xFF)<<16)+((descriptor[9]&0xFF)<<8)+(descriptor[10]&0xFF));
        if (ES_multi_lingual_flag){
            ISO_639_language_code_2=(((descriptor[11]&0xFF)<<16)+((descriptor[12]&0xFF)<<8)+(descriptor[13]&0xFF));
            try {
                text = new String(descriptor, 14, descriptor.length - 14, "EUC-JP");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ARIB_Audio_Component_Descriptor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            try {
                text = new String(descriptor, 11, descriptor.length - 11, "EUC-JP");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ARIB_Audio_Component_Descriptor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void analyzeDescriptor(int DescriptorTag, byte[] descriptor) {

        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * 音声のコンポーネント種別を示す。
     * @return the compornent_type
     * -1:1/0(シングルモノラル)
     * -2:1/0+1/0(デュアルモノラル)
     * -3:2/0
     * -4:2/1
     * -7:3/1
     * -8:3/2
     * -9:3/2+LFE (5.1chサラウンド)
     */
    public int getCompornent_type() {
        return compornent_type;
    }

    /**
     * @return the compornent_tag
     */
    public int getCompornent_tag() {
        return compornent_tag;
    }

    /**
     * @return the stream_type
     */
    public int getStream_type() {
        return stream_type;
    }

    /**
     * @return the ES_multi_lingual_flag
     */
    public boolean isES_multi_lingual_flag() {
        return ES_multi_lingual_flag;
    }

    /**
     * @return the main_component_flag
     */
    public boolean isMain_component_flag() {
        return main_component_flag;
    }

    /**
     * @return the quality_indicator
     */
    public int getQuality_indicator() {
        return quality_indicator;
    }

    /**
     * サンプリング周波数を示す。
     * @return the sampling_rate
     * 1-16kHz
     * 2-22.05kHz
     * 3-24kHz
     * 5-32kHz
     * 6-44.1kHz
     * 7-48kHz
     */
    public int getSampling_rate() {
        return sampling_rate;
    }

    /**
     * @return the ISO_639_language_code
     */
    public int getISO_639_language_code() {
        return ISO_639_language_code;
    }

    /**
     * @return the ISO_639_language_code_2
     */
    public int getISO_639_language_code_2() {
        return ISO_639_language_code_2;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }
    

}
