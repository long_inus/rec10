#!/bin/sh
# Create RPM files for rec10

RPMBUILD=`mktemp -d $HOME/rpmbuild.XXXXXX`
#RPMMACRO=`mktemp    $HOME/.rpmmacros.XXXXXX`
echo "Build directory $RPMBUILD"
mkdir -p $RPMBUILD/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
mkdir rec10-0.9.10
cp -r ./* rec10-0.9.10/
tar zcvf rec10-0.9.10.tar.gz ./rec10-0.9.10
cp rec10-0.9.10.tar.gz $RPMBUILD/SOURCES
cp rec10.spec $RPMBUILD/SPECS
mv ~/.rpmmacros ~/.rpmmacros.old
echo "%_topdir $RPMBUILD" > ~/.rpmmacros
rpmbuild -ba "$RPMBUILD/SPECS/rec10.spec"
cp $RPMBUILD/RPMS/noarch/* .
cp $RPMBUILD/RPMS/`arch`/* .
cp $RPMBUILD/SRPMS/* .
mv ~/.rpmmacros.old ~/.rpmmacros
rm -r rec10-0.9.10/
rm rec10-0.9.10.tar.gz
echo 'Remove build directory'
rm -r $RPMBUILD
#rm $RPMMACRO

