# Rec10 -Ts Recording system- 
Ver.0.9.10  
2014/03/31         Yukikaze & long.inus  
Copyright (C) 2009-2014 Yukikaze & long.inus  

本ソフトは主にpythonおよびperlによって書かれた録画ツール群です。  
ライセンスは　LGPL v3に準拠します(license.txt参照)  
---
##必要環境
###[必要環境:本体部分]
* python(2.6,2.7にて動作確認)
* wine
* mencoder
* ffmpeg
* x264
* mp4box
* *(L-SMASH使用時)*L-SMASH
* *(L-SMASH使用時)*L-SMASH対応x264
* MySQL
* MySQLdb(MySQL-pythonなどの名前)
* *(mkv使用時)*mkvmerge
* java(jre1.6で動作を確認)

###[必要環境:Webインターフェース部分]
* perl
* 一部のperlモジュール

###[必要環境:録画部分]
####同梱していないのもの
* b25(not included)
* *(dvb版ドライバを使わない場合)*recpt1(recfriioでもいいはずです。)
* MySQLdb
####同梱のもの
* jTsSplitter
* BonTsDemux
* epgdump(rec10対応改造版)
* tunerec
---

##インストール(rpmを作成)
1. b25(arib25)をwebからインストール
2. rpm-build,glibc,bash,gcc,kernel-headers,automake等をインストール
3. bash makerpm.sh
4. 出来たrpmファイルをまとめてインストール
   *RHELにおけるepel等の外部リポジトリが依存関係上必要な場合が多いです
5. /etc/rec10.conf等を確認
6. *(dvb版を使う場合)*rec10を実行するユーザーをvideoグループに加える
7. rec10を実行するユーザーで/usr/local/bin/rec10を実行
8. cronに書き加える
9. web-cgi(/var/www/html/rec10にインストールされます)関係の設定を行う
##インストール
1. install.shを実行
2. 表示にしたがって設定の変更(チャンネルなど)
3. make
4. su
5. make install
6. *(dvb版を使う場合)*rec10を実行するユーザーをvideoグループに加える
7. rec10を実行するユーザーで/usr/local/share/rec10を実行
8. cronに書き加える
9. rectool.plをインストール(Makefile.PLの実行 rectool.plの配置)

###*付け足し*

####7.cronの設定

最後にrec10d.pyをcronで五分おきに実行するようにしてください  
crontab -e を実行するとcronを編集できるようになるので  
`*/5 * * * * python /usr/local/bin/rec10`  
のように書いてください(5分ごとに実行)  
  
####8.rectoolのインストール
Makefile.PLを実行します。  
実行した後にrectool.plをhttpの公開ディレクトリ(wwwフォルダなど)に置いてください。  
該当ディレクトリでperlのスクリプトを実行できる必要があります。

####エンコードの設定
エンコードの設定を変えたい場合はts2x264.pyを編集してください

####チャンネルのスキャン
rec10ではチャンネルスキャンおよび各チャンネルが視聴可能かどうかを自動で
インストール時にスキャンします。そのため*チャンネル情報を自分で書く必要はありません*  
  
しかし、契約を変更した、引っ越したなどチャンネルの設定を変えた場合は  
chdata.pyを実行してください  
`python chdata.py`  
番組表が更新されます

####**エンコードを速くする/ファイル容量を小さくする**
mencoder,ffmpeg,x264を最新版にすると、*安定性や圧縮率、処理速度の向上*が見られます。  
最新版をビルドして、/etc/rec10.confの設定部分に実行パスを設定してください。
---

バグだらけのコードですが、改善点などを指摘していただけると幸いです。  
[開発者サイト](http://www.rec10.org "開発者ブログ")  

---
##rec10オプション一覧

* サイズ : S(720x480)/W(854x480)/H(1280x720)/F(1920x1080)/I(Interlaced)
* 画質(x264 のcrf, 小さい方が高画質) : u(+2)/i(+1)/default(設定値)/o(-1)/p(-2)
* 圧縮率(x264 のpreset) : q(veryfast)/w(fast)/default(medium)/e(slow)/r(slower)
* コンテナ : m(mkv mkvmerge が必要)/default(設定値)/4(mp4 MP4Box が必要)
* 音声 : d(二ヶ国語)/default(通常音声)/5(5.1ch) b(ステレオBonTsDemux使用、NHKの一部など)
* フレームレート : a(24fps)/default(30fps)
* 移動opt : R(録画後移動)/D(復号後移動)/E(エンコード後移動)/default(移動なし)
* その他 : N(自動命名オプション) / t(作業途中ファイルを残す)
* 特定デバイス向け : B(Blu-ray)
---

##動作確認のとれたディストリ
* 13/X/X   SVN Centos 6.5(dvb)
* 13/03/22 SVN Centos 6.4(dvb,yukikaze)
* 12/01/08 SVN Debian6.0
* 11/03/10 SVN Centos 5.5 (yukikaze)
* 11/03/10 0.9.9.1+a ArchLinux (yukikaze)
* 11/02/20 0.9.9.1+a Debian 6.0
* 11/02/XX 0.9.9 Ubuntu 10.10
* 10/12/XX 0.9.8 Centos 5.5 (yukikaze)
* 10/XX/XX 0.9.8 Fedora13 (long.inus)


##[History]
* 14/03/31 specファイルの作成(RHEL6,Fedora20,OpenSUSE13.1でテスト)
* 14/01/02 L-Smashに対応
* 14/01/01 モバイル向け追加エンコードオプションを削除
* 13/XX/XX 0.9.10 DVB版の実装を修正
* 12/XX/XX SVN
add Bluray compatible option("B")  
ステレオ音声時のBonTsDemux仕様オプション追加("b")  
lameの実行パスチェックを実装  
mencoderのコマンドラインを最新版対応へ  
ffmpegのオプション追加  
epggenreを追加  
keywordの重複チェックをより賢く  
logoのインポートに対応  
ArchLinuxサポートのための細かな変更  
新BSチャンネル対応  
lavf有効x264でのエラーに対応  
DVB版のドライバに対応  
PMT処理のバグを修正(jTsSplitter)  
DVB版のBS放送のtsid変更に対応  

* 11/02/02 0.9.9
BonTsDemuxを最新版へ(nogui化によりXvfbが不要)  
一部CSチャンネルの追加  
epg_chを変更(ontvの削除)  
チャンネルスキャン機能を実装  
キーワード予約の自動実行オプション追加(in_auto_jbkにauto,optの追加)  
番組初頭の解像度変更に対応  
キーワード予約自動実行オプションの重複チェック実装  
延長対応機能の修正  
移動後のオプション引き継ぎに対応  

* 10/10/09 0.9.8 release
インターレース保持エンコードに対応  
色空間を修正  
放送大学のマルチチャンネルに仮対応  
verboseモードの実装  
ログレベルの実装  
画面サイズが取得できなかったときの処理を追加  
python2.5/2.7に対応  
音声の自動復帰を修正  

* 10/07/10 0.9.7 release
ヒストリーチャンネルのタイトルを修正  
ffmpeg使用時にscale拡大をするように修正  
一時ファイル削除機能の修正  
sarの変更を実装  
最新版のCaption2Assに対応  

* 10/05/29 0.9.6 release
バグ修正  
ログシステムの修正  

* 0.9.5(内部リリース)
b25自動削除機能の改善  
自動仕分け機能のアップデート  
Caption2Assやaac取り出しがうまくいかないときの自動復帰処理の実装  
バグ修正  

* 10/04/02 0.9.4 release
バグ修正  
インストーラーの大幅な改善  
ログシステムの変更  
エンコードの変更  
チャンネル変更に対応(CS)  

* 10/03/20 0.9.3 release(internal)
lots of bug fixes.  
change DB.  
implement updating function(from 0.9.1 or 0.9.2)  
10/03/06 0.9.2 release(internal)  
few bug fix.(!!! amazing i think)  
implement new encoding system.  
use x264cli and mencoder throwgh fifo.  
implement final production changing system(you can choose mkv and mp4 now.)  

* 10/02/21 0.9.1 release
Lots of bug fixes.  
change video filter.  
change encode option.  
implement installer.  

* 09/12/07 0.9.0 release
change config path.(config.ini to rec10.conf)  
add Makefile  
change large number of var name.  
support b25_remote(thanks long.inus)  
support auto copy function.  
change video filter.  
change encode option.  
fix a lot of bugs.  
finish to support iEPG(iRec10EPG).  

* 09/10/27 0.8.0 relaase
Lots of bug fixes.  
Use mkvmerge to mux avi to mkv.  
Many changes.  

* 09/08/22 0.6.0 release
Lots of bug fixes.  
Implement db using MySQL.  
Change Ts splitting soft from Tssplitter to tssplitter_lite(included.)  

* 09/08/01 0.5.0 release
バグフィックス  
検索録画のパターンマッチングアルゴリズムの変更(推測的な検索ができるようになった)  
rectool.plを同梱するようにした。  
周辺ソフトのうちライセンスの問題がないものを同梱するようにした。  

* 09/07/01 0.4.1 release
lots of bug fixes  
add some cs-ch.  
release pl src.  
* 09/05/24 0.3.1 release  
fix  
チャンネルの増加  
エンコードオプションを実際に使えるようにした。  
1passモード実装  
niceコマンドをより広範囲に適用するようにした。  
* 09/05/08 0.1.1 release
fix  
2passエンコードのログファイルがかぶるのを訂正  
一部チャンネル指定が間違っていたのを訂正  
その他多くのバグの訂正  
* 09/05/05 0.0.1b release
fix  
replace tab to 4 spaces(it caused errors)  
add some cs-e2 channels  
* 09/05/04 0.0.1a release  
