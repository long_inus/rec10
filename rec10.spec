%define debug_package %{nil}
%define _unpackaged_files_terminate_build 0

%if 0%{?suse_version}
	%define http_dir /srv/www/html
%else
	%define http_dir /var/www/html

Summary:	rec10 - yet another hoge -
Name:		rec10
Version:	0.9.10
Release:	38
Group:		Applications/Multimedia
Source:		%{name}-%{version}.tar.gz
Vendor:		rec10
License:	LGPL ver.3
URL:		http://www.rec10.org/
BuildRoot:	%{_tmppath}/%{name}-%{version}-buildroot
BuildRequires:	glibc kernel-headers gcc make bash
Requires(pre):	shadow-utils /usr/sbin/usradd /usr/bin/getent
Requires(postun): /usr/sbin/userdel
Requires:	python > 2.6 wine mencoder x264 gpac mysql MySQL-python bash
AutoReqProv:	yes
Provides:	rec10 = 0.9.10

%description
rec10 is a ts recording tools for Japanese terrestrial digital broadcasting system.

%package tools
Summary: tools for rec10
Group: Applications/Multimedia
AutoReqProv:	yes
%description tools
tstools for rec10. It include files listed below.
BonTsDemux - Separate multi audio channel ts streams into every audio files.It runs on wine.
epgdump - epg dumping tool.
jTsSplitter - Ts splitter written in java.

%package selinux
Summary: 	selinux related files
Requires:	rec10 = %{version}-%{release} rec10-www = %{version}-%{release} rec10-tools = %{version}-%{release}
Requires(post):	policycoreutils-python
Requires(postun):	policycoreutils-python
AutoReqProv:	yes
%description selinux
This rpm change selinux bool option httpd_can_network_connect_db to 1.

%package www
Summary: tools for rec10
Group: Applications/Multimedia
Requires:	perl-Algorithm-Diff perl-Archive-Zip perl-CGI perl-Config-Simple perl-Data-Dumper-Concise
Requires:	perl-Date-Simple perl-DateTime perl-DBI perl-File-Slurp perl-Sort-Naturally perl-SVG
Requires:	perl-Time-Piece perl-Tie-IxHash perl-List-Compare perl-XML-Atom perl-XML-Generator-DBI perl-XML-SAX-Writer perl-XML-TreePP
AutoReqProv:	no
%description www
rectool-0.0.1


%prep
rm -rf $RPM_BUILD_ROOT

%setup


%build
#bash install.sh
cd tstools
make CFLAGS="-std=c99 -O2 -Wall $RPM_OPT_FLAGS" CXXFLAGS="-O2 -Wall -g -lstdc++ $RPM_OPT_FLAGS"
cd ../tunerec
make CFLAGS="-std=c99 -O2 -Wall -pthread -g $RPM_OPT_FLAGS"
cd ../rec10
make
#python2 ./install.py
cd ../

%pre
	/usr/bin/getent passwd rec10 || /usr/sbin/useradd -r -g video -s /sbin/nologin -c "user for rec10" rec10
exit 0

%install
%__mkdir -p $RPM_BUILD_ROOT/usr/local/share/rec10
%__mkdir -p $RPM_BUILD_ROOT/usr/local/bin
%__mkdir -p $RPM_BUILD_ROOT/etc
%__mkdir -p $RPM_BUILD_ROOT/var/tmp/rec10
%__mkdir -p $RPM_BUILD_ROOT/var/log
%__mkdir -p $RPM_BUILD_ROOT/var/rec10/recording
cd rec10
make install PREFIX=$RPM_BUILD_ROOT/usr/local CONF=$RPM_BUILD_ROOT/etc TEMP=$RPM_BUILD_ROOT/var/tmp LOG=$RPM_BUILD_ROOT/var/log
cd ../tunerec
make install PREFIX=$RPM_BUILD_ROOT/usr/local
cd ../tstools
make install PREFIX=$RPM_BUILD_ROOT/usr/local
%__mkdir -p $RPM_BUILD_ROOT/usr/local/share/rec10/rectool/
%__mkdir -p $RPM_BUILD_ROOT/etc/httpd/conf.d/
%__mkdir -p $RPM_BUILD_ROOT/var/www/html/rec10/
cd ../rectool
%__cp rectool.pl $RPM_BUILD_ROOT/usr/local/share/rec10/rectool/
%__cp rec10.conf $RPM_BUILD_ROOT/etc/httpd/conf.d/
#ln -sf $RPM_BUILD_ROOT/usr/local/share/rec10/rectool/rectool.pl $RPM_BUILD_ROOT/var/www/html/rec10/
ln -sf /usr/local/share/rec10/rectool/rectool.pl $RPM_BUILD_ROOT/var/www/html/rec10/

%clean
rm -rf $RPM_BUILD_ROOT

%postun
/usr/sbin/userdel rec10

%post selinux
semanage fcontext -a -t httpd_sys_rw_content_t '/var/rec10(/.*)?' 2>/dev/null || :
semanage fcontext -a -t httpd_sys_script_exec_t '%{http_dir}/rec10(/.*)?' 2>/dev/null || :
restorecon -R /var/rec10 || :
restorecon -R %{http_dir}|| :
setsebool -P httpd_can_network_connect_db 1

%postun selinux
if [ $1 -eq 0 ] ; then
	semanage fcontext -d -t httpd_sys_rw_content_t '/var/rec10(/.*)?' 2>/dev/null || :
	semanage fcontext -d -t httpd_sys_script_exec_t '%{http_dir}/rec10(/.*)?' 2>/dev/null || :
	setsebool -P httpd_can_network_connect_db 0
fi

%files
%defattr(-,rec10,video)
%config(noreplace) /etc/rec10.conf
/usr/local/share/rec10/*.py
/usr/local/share/rec10/*.pyc
/usr/local/share/rec10/*.pyo
%attr(775,rec10,video)/usr/local/share/rec10/rec10
%attr(777,rec10,video)/var/log/rec10
/var/rec10/recording
%attr(775,rec10,video)/usr/local/bin/rec10
%attr(775,rec10,video)/usr/local/bin/tunerec
%attr(775,rec10,video)/var/tmp/rec10
%files tools
%attr(775,rec10,video)/usr/local/bin/epgdump
/usr/local/share/rec10/tstools/BonTsDemux.exe
/usr/local/share/rec10/tstools/BonTsDemux.txt
/usr/local/share/rec10/tstools/src.zip
/usr/local/share/rec10/tstools/BonTsDemuxLib.dll
/usr/local/share/rec10/tstools/jTsSplitter.jar
%attr(775,rec10,video)/usr/local/bin/jtssplitter

%files www
%defattr(-,root,root,-)
%doc
/usr/local/share/rec10/rectool/rectool.pl
%config(noreplace)/etc/httpd/conf.d/rec10.conf
%attr(755,apache,apache)/var/www/html/rec10/

%files selinux

%changelog
* Tue Apr 06 2014 gn64 <gn64@rec10.org>
- add user(rec10:video) creation.
* Tue Apr 01 2014 gn64 <gn64@rec10.org>
- change tmp path /tmp/rec10 to /var/tmp/rec10
- change file permissions
* Wed Mar 26 2014 gn64 <gn64@rec10.org>
- make spec file.