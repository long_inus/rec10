#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import glob
import os
import os.path
import re
import time
import shutil
import traceback
import sys

import recdblist
import guess
import configreader

def getTitle(title):
    rT=re.compile("(.+)_(\d+)\Z")
    tT=rT.match(title)
    rT2=re.compile("(.+)_(.+)_(\d*)\Z")#_(aichi)_2010-02-06T01:59:00.mkv
    tT2=rT2.match(title)
    rT3=re.compile("(.+)_(.+)_\d+-\d+-\d+T\d+:\d+:\d+\Z")#_(aichi)_2010-02-06T01:59:00.mkv
    tT3=rT3.match(title)
    rT4=re.compile("(.+)_(.+)_\d+-\d+-\d+T\d+-\d+-\d+\Z")#_(aichi)_2010-02-06T01-59-00.mkv
    tT4=rT4.match(title)
    rT5=re.compile("(.+)_(.+)_(.+)\Z")#_(aichi)_2010-02-06T01-59-00.mkv
    tT5=rT5.match(title)
    ntitle=title
    if tT :
        ntitle=tT.group(1)
    elif tT2:
        ntitle=tT2.group(1)
    elif tT3:
        ntitle=tT3.group(1)
    elif tT4:
        ntitle=tT4.group(1)
    elif tT5:
        ntitle=tT5.group(1)
    return ntitle
def searchFile(temppath,recpath,ext):
    """
    録画一時フォルダ内ファイルを検索
    """
    avilist = glob.glob(temppath + "/*"+ext)
    ret=[]
    for avif in avilist:
        ##b25f is title.ts.b25  avi is title.avi
        dir = os.path.split(avif)[0]
        title = os.path.split(avif)[1]
        title = title.replace(ext, "")
        avipath = os.path.join(dir, title + ext)
        if os.path.exists(avipath):
            dtime = time.time()-os.path.getmtime(avipath)
            dtime = int(dtime)
            if dtime > 300:
                if veryfySize(avipath):
                    ret.append(title)
    return ret
def getMoveDestpath(title,temppath,recpath,ext):
    dstpath=os.path.join(recpath,title+ext)
    srcpath=os.path.join(temppath,title+ext)
    if os.path.exists(dstpath):
        if not os.path.getsize(dstpath) == os.path.getsize(srcpath):
            gmtime=time.gmtime(os.path.getmtime(srcpath))
            iff=""
            try:
                iff=u"("+configreader.getConfEnv("iff")+u")_"
                if iff==u"()_":
                    iff = u""
            except:
                iff=""
            title=title+u"_"+iff+time.strftime("%Y-%m-%dT%H-%M-%S",gmtime)
        else:
            recdblist.printutf8(u"同サイズのファイルが存在します")

    return title
def getDelpath(temppath,title,addfp=1):
    delpath=[os.path.join(temppath,title+".ts")]
    delpath.append(os.path.join(temppath,title+".avi"))
    if addfp==1:
        delpath.append(os.path.join(temppath,title+".mp4"))
        delpath.append(os.path.join(temppath,title+".mkv"))
    delpath.append(os.path.join(temppath,title+".m2v"))
    delpath.append(os.path.join(temppath,title+".120.avi"))
    delpath.append(os.path.join(temppath,title+".timecode.txt"))
    delpath.append(os.path.join(temppath,title+".aac"))
    delpath.append(os.path.join(temppath,title+".m4v"))
    delpath.append(os.path.join(temppath,title+".old.m4v"))
    delpath.append(os.path.join(temppath,title+".m4a"))
    delpath.append(os.path.join(temppath,title+".ts.b25"))
    delpath.append(os.path.join(temppath,title+".ts.tsmix"))
    delpath.append(os.path.join(temppath,title+".ts.log"))
    delpath.append(os.path.join(temppath,title+".sa.avi"))
    delpath.append(os.path.join(temppath,title+".sa.avi.log"))
    delpath.append(os.path.join(temppath,title+".log"))
    delpath.append(os.path.join(temppath,title+".log.zip"))
    return delpath
def veryfySize(path):
    #vsize=[297,497,596,1196]#SD 30m 1h 1.5h 2h
    vsize=[245,275,295,591,830]
    vsize=vsize+[325,449,560,590,602,690,805,860,1014,1138,1237]
    vsize=vsize+[261,535,540,616,740]#HD 30m 1h
    #vsize=vsize+[381,895,447]
    ret = 0
    for size in vsize:
        if os.path.getsize(path)>(size-10)*1024*1024 and os.path.getsize(path)<(size+10)*1024*1024:
            ret=1
    if os.path.getsize(path)>270*1024*1024:
        ret=1
    return ret
def execMove(title,temppath,recpath,ext,autodel):
    srcpath=os.path.join(temppath,title+ext)
    # patched. orig: searchFolder(title, recpath)
    sf=guess.searchFolder(title, recpath,200)
    if sf!="":
        destpath=os.path.join(sf,getMoveDestpath(title, temppath, sf, ext)+ext)
        if os.path.exists(destpath):
            if os.path.getsize(destpath) == os.path.getsize(srcpath):
                recdblist.printutf8(u"同名同サイズのファイルが存在します。")
                if autodel==1:
                    recdblist.printutf8(u"関連ファイルを削除します。")
                    delpath=getDelpath(temppath, title)
                    for dp in delpath:
                        try:
                            os.remove(dp)
                            ""
                        except:
                            ""
        else:
            recdblist.printutf8("moving now..")
            recdblist.printutf8(srcpath+" : "+destpath)
            print srcpath
            shutil.copy(srcpath, destpath)
            #shutil.copy(srcpath, destpath)
            if autodel==1:
                delpath=getDelpath(temppath, title)
                for dp in delpath:
                    try:
                        os.remove(dp)
                        ""
                    except:
                        ""
    else:
        recdblist.printutf8("sf not fonud @ execMove",verbose_level=100)

def execDelete(title,temppath):
    delpath=getDelpath(temppath, title,0)
    recdblist.printutf8(title+u"　関連の一時ファイルを削除します")
    for dp in delpath:
        try:
            if os.path.exists(dp):
                recdblist.printutf8(dp)
                os.remove(dp)
        except Exception, inst:
            print type(inst)
            print str(inst)
            print traceback.print_exc(file=sys.stdout)
