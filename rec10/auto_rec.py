#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import n_gram
import rec10d

def getDBKey(key,chtxt):
    return rec10d.rec10db.select_by_key_in_auto_bayes_key(key, chtxt)

def changeDBEpg(chtxt,beforenum,newnum):
    rec10d.rec10db.change_ratio_all_reduce_in_auto_bayes_key(chtxt, beforenum, newnum)

def changeDBNum(chtxt,recaddnum,alladdnum):
    """
    add num.(Not change)
    """
    rec10d.rec10db.add_num_in_auto_bayes_key(chtxt, recaddnum,alladdnum)

def changeDBRatioRec(key,chtxt,beforenum,addnum):
    rec10d.rec10db.change_ratio_rec_in_auto_bayes_key(key, chtxt, beforenum, addnum)

def changeDBRatioMultiRec(chtxt,beforenum,list):
    rec10d.rec10db.change_multi_ratio_rec_in_auto_bayes_key(chtxt, beforenum, list)

def changeDBReduceRatioRec(chtxt,beforenum,addnum):
    rec10d.rec10db.change_ratio_rec_reduce_in_auto_bayes_key(chtxt, beforenum, addnum)

def changeDBRatioAll(key,chtxt,beforenum,addnum):
    rec10d.rec10db.change_ratio_all_in_auto_bayes_key(key, chtxt, beforenum, addnum)

def changeDBRatioMultiAll(chtxt,beforenum,list):
    rec10d.rec10db.change_multi_ratio_all_in_auto_bayes_key(chtxt,beforenum, list)

def getDBNum(chtxt):
    ret=getDBKey("NUM", chtxt)
    if len(ret)<3:
        rec10d.rec10db.add_in_auto_bayes_key("NUM", chtxt, 1,1)
        ret=getDBKey("NUM", chtxt)
    return ret

def updateRecall(chtxt,titles,descs,newnum):
    """
    update recall
    """
    titles=u" %s " %titles
    descs=u" %s " %descs
    str1=titles*2+descs+titles*2
    noun=n_gram.getNounQuadGram(str1)
    bnum=getDBNum(chtxt)[3]
    if bnum<1:
        bnum=100
    changeDBRatioMultiAll(chtxt, bnum,noun)
    changeDBEpg(chtxt, bnum, newnum)
    changeDBNum(chtxt, 0, newnum)

def addKey(chtxt,title,desc):
    title=u" %s " %title
    desc=u" %s " %desc
    str1=title*2+desc+title*2##タイトルは重視したいので幾度か足す。
    noun=n_gram.getNounQuadGram(str1)#nounは辞書のキーに文字を、中身に出現回数を書いたもの。
    bnum=getDBNum(chtxt)[2]
    if bnum<1:
        bnum=1
    changeDBRatioMultiRec(chtxt, bnum, noun)
    changeDBNum(chtxt,1,0)
    changeDBReduceRatioRec(chtxt, bnum,1)
def calcKey(chtxt,title,desc):
    """

    """
    title=u" %s " %title
    desc=u" %s " %desc
    str1=title*2+desc+title*2##タイトルは重視したいので幾度か足す。
    noun=n_gram.getNounQuadGram(str1)#nounは辞書のキーに文字を、中身に出現回数を書いたもの。
    tnum=1
    nump=0
    for key,num in noun.items():
        pp=getDBKey(key,chtxt)
        pp2=getDBKey(key,"ALL")
        if len(pp)>3:
            tarec=0
            taall=0
            if len(pp2)>3:
                tarec=pp2[2]
                taall=pp2[3]
            trec=pp[2]
            tall=pp[3]
            
            tnum=tnum+1
            p=(1000*trec+5)/(1000*tall+5)
            p=p*p*100
            p2=(1000*tarec+5)/(1000*taall+5)
            p2=p2*100
            nump=nump+p+p2
    return int(nump/tnum)
