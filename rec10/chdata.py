#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import os.path
import configreader
import xml.dom.minidom
import scan_ch
import datetime

import rec10d

path = str(os.path.dirname(os.path.abspath(__file__))) + "/"

def chAdd(bctype, chtxt, ch, csch,tsid="0"):#すべて文字列
    tnow=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    rec10d.rec10db.add_epg_ch(bctype,chtxt, ch, csch,tsid, u"2010-01-01 00:00:00",u"2010-01-01 00:00:00",tnow)
def addCHlist(cl):
    if len(cl)>0:
        for c in cl:
            chAdd(c[0],c[1],c[2],c[3])
def getChList(xmlpath):
    dom=xml.dom.minidom.parse(file(xmlpath))
    chlist=[]
    for ch in dom.getElementsByTagName('channel'):
        bctype=ch.getAttribute("bctype")
        chd=ch.getAttribute("ch")
        csch=ch.getAttribute("csch")
        chtxt=ch.getAttribute("chtxt")
        ontv=ch.getAttribute("ontvcode")
        chlist.append([bctype,ontv,chtxt,chd,csch])
    return chlist
def deleteChListALL():
    rec10d.rec10db.new_epg_ch()
if __name__ == "__main__":
    deleteChListALL()
    scan_ch.searchCh()
