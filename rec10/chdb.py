#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2012 Yukikaze
    # モジュール属性 argv を取得するため
import rec10d

def searchCHFromChtxt(chtxtin):
    chtxtt=chtxtin.replace("_0","_%")
    ret = []
    for datum in rec10d.rec10db.select_by_chtxt_epg_ch(chtxtt):
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4],'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret[0]
def searchCHFromBctype(bctypein):
    ret = []
    for datum in rec10d.rec10db.select_by_bctype_epg_ch(bctypein):
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4], 'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret[0]
def searchCHFromCh(chin):
    ret = []
    for datum in rec10d.rec10db.select_by_ch_epg_ch(chin):
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4], 'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret[0]
def searchCHFromCsch(cschin):
    ret = []
    for datum in rec10d.rec10db.select_by_csch_epg_ch(cschin):
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4], 'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret[0]
def searchAllCHFromCh(chin):
    ret = []
    for datum in rec10d.rec10db.select_by_ch_epg_ch(chin):
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4], 'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret
def getAllCH():
    ret = []
    for datum in rec10d.rec10db.select_all_epg_ch():
        rett = {'bctype':datum[0], 'chtxt':datum[1], 'ch':datum[2], 'csch':datum[3],'tsid':datum[4], 'update':datum[5], 'chname':datum[6], 'visible':datum[8]}
        ret.append(rett)
    if len(ret) == 0:
        ret.append(None)
    return ret
def update():
    ret = []
    CSupdate = 0
    CS2update = 0
    BSupdate = 0
    rec10d.rec10db.set_new_status("8")
    for datum in rec10d.rec10db.select_get_update_epg_ch("6"):
        bctype = datum[0]
        if bctype == "cs1" or bctype == u"cs1":
            if CSupdate == 0:
                ret.append(u"cs1")
                CSupdate = 1
        elif bctype == "cs2" or bctype == u"cs2":
            if CS2update == 0:
                ret.append(u"cs2")
                CS2update = 1
        elif bctype == "bs" or bctype == u"bs":
            if BSupdate == 0:
                ret.append(u"bs")
                BSupdate = 1
        else:
            ret.append(bctype)
    return ret
def changeCHShow(chtxt,isshow):
    rec10d.rec10db.change_visible_epg_ch(chtxt, isshow)
def updateLogo():
    ret=[]
    BSCSUpdate=0
    for datum in rec10d.rec10db.select_get_updatelogo_epg_ch("720"):
        bctype=datum[0]
        if bctype == "cs1" or bctype == u"cs1":
            if BSCSUpdate==0:
                ret.append(u"bs")
                BSCSUpdate=1
        elif bctype == "cs2" or bctype == u"cs2":
            if BSCSUpdate==0:
                ret.append(u"bs")
                BSCSUpdate=1
        elif bctype == "bs" or bctype == u"bs":
            if BSCSUpdate==0:
                ret.append(u"bs")
                BSCSUpdate=1
        else:
            ret.append(bctype)
    return ret
