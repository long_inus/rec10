#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import os
import os.path
import time

def checkB25Decode(b25filepath, decodedfilepath , csch):
    ret = 0
    if os.path.isfile(decodedfilepath):##tsファイルが存在している
        dtime = time.time()-os.path.getmtime(decodedfilepath)
        dtime = int(dtime)
        if dtime > 20:
            if csch>0:
                if (compareFilesize(b25filepath, decodedfilepath, 10) and minimizeCheck(decodedfilepath, 800)):
                    ret = 1
            else:
                if (compareFilesize(b25filepath, decodedfilepath, 60) and minimizeCheck(decodedfilepath, 800)):
                    ret = 1
        else:
            time.sleep(30)
            dtime = time.time()-os.path.getmtime(decodedfilepath)
            dtime = int(dtime)
            if dtime > 20:
                if csch>0:
                    if (compareFilesize(b25filepath, decodedfilepath, 10) and minimizeCheck(decodedfilepath, 800)):
                        ret = 1
                else:
                    if (compareFilesize(b25filepath, decodedfilepath, 60) and minimizeCheck(decodedfilepath, 800)):
                        ret = 1
    #recdblist.printutf8("deleting b25 is " + str(ret))
    return ret

def compareFilesize(pathbase, path, percentage):
    """
    return true if size of path is larger than that of pathbase.
    percentage is int(0< <100)
    """
    if not os.path.exists(pathbase):
        return 0
    basesize = os.path.getsize(pathbase)
    pathsize = os.path.getsize(path)
    size = pathsize * 100-basesize * percentage
    if size < 0:
        return 0
    else:
        return 1
    
def minimizeCheck(filepath, minsize):
    """
    minsize is Megabytes
    """
    if os.path.getsize(filepath) > minsize * 1000 * 1000:
        return 1
    else:
        return 0
