#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

#rec10タスクのtype

import configreader

REC_RESERVE = "reserve_flexible"
REC_FINAL_RESERVE = "reserve_fixed"
REC_ENCODE_GRID = "convert_ts_mp4_network"
REC_ENCODE_LOCAL = "convert_ts_mp4_running"
REC_ENCODE_QUE = "convert_ts_mp4"
REC_MISS_ENCODE = "convert_avi_mp4_miss"
REC_KEYWORD = "search_today"
REC_KEYWORD_EVERY_SOME_DAYS = "search_everyday"
REC_FIN_LOCAL = "convert_ts_mp4_finished"
REC_MISS_DECODE = "convert_b25_ts_miss"
REC_TS_DECODE_QUE = "convert_b25_ts"
REC_TS_DECODING = "convert_b25_ts_running"
REC_TS_RECORDING = "reserve_running"
REC_CHANGING_CANTAINER = "convert_mkv_mp4_running"
REC_AVI_TO_MKV = "convert_avi_mkv"
REC_AVI_TO_MP4 = "convert_avi_mp4"
REC_MKV_TO_MP4 = "convert_mkv_mp4"
REC_AUDIO_VIDEO_MUX = "mux_audio_video"
REC_AUDIO_VIDEO_MUXING = "mux_audio_video_running"



#ここから処理のちに移動

REC_MOVE_END = "move_end"

#ここから自動で提起される処理。

REC_AUTO_SUGGEST_REC = "auto_suggest_rec"
REC_AUTO_SUGGEST_DECODE = "auto_suggest_dec"
REC_AUTO_SUGGEST_ENCODE = "auto_suggest_enc"
REC_AUTO_SUGGEST_AVI2FP = "auto_suggest_avi2fp"
REC_AUTO_SUGGEST_AP2FP = "auto_suggest_ap2fp"
REC_AUTO_KEYWORD = "auto_keyword"
REC_BAYES_SUGGEST ="bayes_suggest"


#拡張子の処理レベル(0からスタート10で終了)

#0:b25 5:ts(del tsmix and ts.b25) 9:x264(del 2 and so on) 10:mp4/mkv
EXT_LEVEL_0 = [u".ts.b25"]
EXT_LEVEL_4 = [u".tsmix",u".ts.tsmix"]
#EXT_LEVEL_5 = [u".ts"]
EXT_LEVEL_9 = [
    u".avi",u".120.avi",u".noodml.avi",u".sa.avi",
    u".264",u".265",u".vp9",
    u".aac",u".wav",u".m2v",u".m4a",u".m4v",
    u"_1.aac",u"_2.aac",u"_1.m4a",u"_2.m4a",
    u"_1.mp3",u"_2.mp3",u"_1.wav",u"_2.wav",
    u".old.mp4",u".old.m4v",
    u".srt",u".ts",
]
EXT_LEVEL_10 = [u".mp4",u".mkv"]

BONTSDEMUX_DELAY="0"

version = 103
version_str="0.9.10"

global log_level_now
global verbose_level_now
try:
    log_level_now=int(configreader.getConfLog("log_level"))
    verbose_level_now=int(configreader.getConfLog("verbose_level"))
except:
    log_level_now=900
    verbose_level_now=400



