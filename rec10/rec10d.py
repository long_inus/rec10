#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import os
import os.path
import warnings
warnings.filterwarnings('ignore', "the sets module is deprecated")
import configreader
import dbMySQL
import timerec

path = str(os.path.dirname(os.path.abspath(__file__))) + "/"
tmppath = configreader.getConfPath("tmp")+"/"
if tmppath=="/":
    tmppath=path
if not os.path.exists(tmppath):
    os.makedirs(tmppath)
    
global rec10db

def main():
    timerec.task()
    
db = configreader.getConfDB("db")
if db == "MySQL":
    dbn = configreader.getConfDB("mysql_dbname")
    dbh = configreader.getConfDB("mysql_host")
    dbu = configreader.getConfDB("mysql_user")
    dbpwd = configreader.getConfDB("mysql_passwd")
    dbport = int(configreader.getConfDB("mysql_port"))
    rec10db = dbMySQL.DB_MySQL(dbname=dbn, host=dbh, user=dbu, passwd=dbpwd, port=dbport)
    rec10db.new_in_status()
else:
    ""
    
if __name__ == "__main__":
    main()

