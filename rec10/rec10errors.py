#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

class AudioCorruptedError(Exception):
    def __init__(self, audiofilename):        # strとptnはraise文から受け取る引数
        self.audiofilename = audiofilename  # メンバ変数に代入
    def __str__(self):                   # エラーメッセージ
        return 'Audio file "%s" looks corrupted.' % self.audiofile