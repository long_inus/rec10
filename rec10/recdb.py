#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
import chdb
import n_gram
import rec10d
import rec10const

def reserveKeyword(keyword, chtxt, btime, etime, deltatime, opt):
    rec10d.rec10db.add_timeline(type=rec10const.REC_KEYWORD, chtxt=chtxt, title=keyword, btime=btime, etime=etime, deltatime=deltatime, opt=opt)
def reserveEverydayKeyword(keyword, chtxt, btime, etime, deltatime, opt, deltaday,count):
    tcount=count
    if tcount<0:
        tcount=-1
    rec10d.rec10db.add_timeline(type=rec10const.REC_KEYWORD_EVERY_SOME_DAYS, chtxt=chtxt, title=keyword, btime=btime, etime=etime, deltatime=deltatime, opt=opt, deltaday=deltaday,counter=tcount)
def reserveReckey(type, title, chtxt, btime, etime, opt):
    rec10d.rec10db.add_timeline(type=type, chtxt=chtxt, title=title, btime=btime, etime=etime, opt=opt)
def deleteReckey(type, title, chtxt, btime):
    rec10d.rec10db.del_timeline(type=type, title=title, chtxt=chtxt, btime=btime)
def reserveAutoKeyword(chtxt,title,btime,etime):
    rec10d.rec10db.add_auto_timeline_keyword(chtxt, title, btime, etime)
def addAutoBayesKeyword(chtxt,title,btime,etime,point):
    rec10d.rec10db.add_auto_timeline_bayes(chtxt, title, btime, etime,point)
def getProgramsInTheseHours(dhour):
    dhour = int(dhour)
    dminutes = 60 * dhour
    dminutes = str(dminutes)
    return rec10d.rec10db.select_bytime_timeline(dminutes)
def getProgramsInTheseMinutes(dminutes):
    return rec10d.rec10db.select_bytime_timeline(dminutes)
def countRecNow(dhour):
    d = getProgramsInTheseHours(dhour)
    ret = 0
    for i in d:
        t = i['type']
        if t == "key" or t == "keyevery" or t == "rec" or t == "res":
            ret = ret + 1
    return ret
def countRecNow_minutes(dminutes):
    d = getProgramsInTheseMinutes(dminutes)
    ret = 0
    for i in d:
        t = i['type']
        if t == "key" or t == "keyevery" or t == "rec" or t == "res":
            ret = ret + 1
    return ret
def countRecNow_minutes_BSCS(dminutes):
    d = getProgramsInTheseMinutes(dminutes)
    ret = 0
    for i in d:
        t = i['type']
        if t == "key" or t == "keyevery" or t == "rec" or t == "res":
            if len(chdb.searchCHFromChtxt(i['chtxt'])['ch']) > 2:
                ret = ret + 1
    return ret
def countRecNow_minutes_TE(dminutes):
    d = getProgramsInTheseMinutes(dminutes)
    ret = 0
    for i in d:
        t = i['type']
        if t == "key" or t == "keyevery" or t == "rec" or t == "res":
            if len(chdb.searchCHFromChtxt(i['chtxt'])['ch']) < 3:
                ret = ret + 1
    return ret
def deleteOldProgramBeforeTheseHours(dhour):
    """
    delete keys except rec10const.REC_MISS_ENCODE and rec10const.REC_KEYWORD_EVERY_SOME_DAYS before dhour hours from now.
    """
    rec10d.rec10db.delete_old_timeline(dhour)
def delete_old_auto_keyword(dhour):
    rec10d.rec10db.delete_old_auto_timeline_keyword(dhour)
def delete_old_auto_bayes(dhour):
    rec10d.rec10db.delete_old_auto_timeline_bayes(dhour)
def getAll():
    return rec10d.rec10db.select_all_timeline()
def addRecLogProgram(title,chtxt,btime,etime,opt,exp,longexp,category):
    rec10d.rec10db.add_in_timeline_log(chtxt,title,btime,etime,opt,exp,longexp,category)
def getAllJbkKeyword():
    return rec10d.rec10db.select_all_in_auto_jbk_key()
def checkDuplicated(title,chtxt,epgbtime,epgetime):
    tl=rec10d.rec10db.select_byepgtime_all_timeline(epgbtime,epgetime)
    nel=rec10d.rec10db.select_bytime_all_timeline(epgbtime, epgetime)
    dup=0
    for t in tl:
        if t["title"]==title and t["chtxt"]==chtxt:
            dup=1
        elif n_gram.trigram(t["title"],title)> 500 and t["chtxt"]==chtxt:
            dup=1
    for t in nel:
        if t["title"]==title and t["chtxt"]==chtxt:
            dup=1
        elif n_gram.trigram(t["title"],title)> 500 and t["chtxt"]==chtxt:
            dup=1
    return dup
