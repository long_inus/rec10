#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import os
import os.path
import time
import commands
import traceback
import datetime

import chdb
import rec10d
import configreader
import recdblist
import tv2ts

tmppath = configreader.getConfPath("tmp")+"/"
if not os.path.exists(tmppath):
    os.mkdir(tmppath)
tssplitterp =configreader.getConfPath("java")+" -jar "+configreader.getConfPath("jTsSplitter")

def searchCh(forced=0):
    if rec10d.rec10db.select_installed_in_status() < 2 or forced > 0:
        rec10d.rec10db.change_installed_in_status(2)
        rec10d.rec10db.new_epg_ch()
        #地上デジタル
        for i in xrange(13,52):#62
            recdblist.printutf8ex(u"チャンネルスキャン:地上デジタル CH "+str(i), 200, 200)
            if checkTs(str(i))>0:
                addCh(str(i),u"te"+str(i),str(i))
        time.sleep(1)
        #BSデジタル
        recdblist.printutf8ex(u"チャンネルスキャン:BSデジタル ", 200, 200)
        for i in xrange(100,299):#240
            recdblist.printutf8ex(u"チャンネルスキャン:BSデジタル CH "+str(i), 200, 200)
            if checkTs(str(i))>0:
                addCh(str(i),u"bs","BS")
        for i2 in xrange(0,11):
            chi=u"BS"+str(2*i2+1)
            chii=2*i2+1
            recdblist.printutf8ex(u"チャンネルスキャン:BSデジタル CH "+chi, 200, 200)
            for ts_offset in xrange(0,4):
                for ts_idx in xrange(0,8):
                    t_tsid=16384+chii*16+ts_offset*512+ts_idx
                    recdblist.printutf8ex(u"TransportID "+str(t_tsid), 200, 200)
                    if checkTs(chi,str(t_tsid))>0:
                        addCh(str(chi),u"bs","BS",str(t_tsid))
        time.sleep(1)
        #スカパーe2!
        #http://www5e.biglobe.ne.jp/~kazu_f/digital-sat/trapon-nsat110.html
        #
        #CS1ネットワーク
        if checkTs("CS2")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS2 ", 200, 200)
            addCh("CS2","cs1","CS")
        if checkTs("CS8")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS8", 200, 200)
            addCh("CS8","cs1","CS")
        if checkTs("CS10")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS10", 200, 200)
            addCh("CS10","cs1","CS")
        #CS2ネットワーク
        if checkTs("CS4")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS4", 200, 200)
            addCh("CS4","cs2","CS")
        if checkTs("CS6")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS6", 200, 200)
            addCh("CS6","cs2","CS")
        if checkTs("CS12")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS12", 200, 200)
            addCh("CS12","cs2","CS")
        if checkTs("CS14")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS14", 200, 200)
            addCh("CS14","cs2","CS")
        if checkTs("CS16")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS16", 200, 200)
            addCh("CS16","cs2","CS")
        if checkTs("CS18")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS18", 200, 200)
            addCh("CS18","cs2","CS")
        if checkTs("CS20")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS20", 200, 200)
            addCh("CS20","cs2","CS")
        if checkTs("CS22")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS22", 200, 200)
            addCh("CS22","cs2","CS")
        if checkTs("CS24")>0:
            recdblist.printutf8ex(u"チャンネルスキャン:CSSデジタル CH:CS24", 200, 200)
            addCh("CS24","cs2","CS")
        rec10d.rec10db.change_installed_in_status(100)
        
def checkTs(ch,tsid="0"):
    if os.path.exists(os.path.join(tmppath,"ch_"+str(ch)+".ts")):
        os.remove(os.path.join(tmppath,"ch_"+str(ch)+".ts"))
    if os.path.exists(os.path.join(tmppath,"ch_"+str(ch)+".ts.b25")):
        os.remove(os.path.join(tmppath,"ch_"+str(ch)+".ts.b25"))
    tv2ts.tv2b25ts(os.path.join(tmppath,"ch_"+str(ch)+".ts"), ch, "1",tsid)
    if os.path.exists(os.path.join(tmppath,"ch_"+str(ch)+".ts.b25")):
        os.remove(os.path.join(tmppath,"ch_"+str(ch)+".ts.b25"))
    if os.path.exists(os.path.join(tmppath,"ch_"+str(ch)+".ts")):
        os.remove(os.path.join(tmppath,"ch_"+str(ch)+".ts"))
        return 1
    else:
        return 0

def addCh(ch,bctype,chtxthead,tsid="0"):
    prglist=getChProgNum(ch,tsid)##[prglist,logt]
    for lstr in prglist[0]:
        #print [bctype,chtxthead+"_"+lstr, ch, lstr]
        try:
            if int(lstr)<1000000:
                if ch.find("bs")>-1:
                    if int(lstr)<300:
                        tnow=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        rec10d.rec10db.add_epg_ch(bctype,chtxthead+"_"+lstr, ch, lstr,tsid, u"2010-01-01 00:00:00", u"2010-01-01 00:00:00",tnow)
                else:
                    tnow=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    rec10d.rec10db.add_epg_ch(bctype,chtxthead+"_"+lstr, ch, lstr,tsid, u"2010-01-01 00:00:00", u"2010-01-01 00:00:00",tnow)
        except Exception, inst:
            recdblist.addCommonlogEX("Warning", "addCh (scan_ch.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
    checkB25Purchased(ch, prglist[1])
    
def getChProgNum(ch,tsid):
    pout=os.path.join(tmppath,"ch_"+str(ch)+".ts")
    if tsid != "0":
        pout=os.path.join(tmppath,"ch_"+str(ch)+"_"+tsid+".ts")
    logt=tv2ts.tv2tsmix(pout, ch, "10",tsid)
    retl=getTsProgNum(ch,pout)
    try:
        os.remove(pout)
    except:
        ""
    return [retl,logt]

def getTsProgNum(ch,tspath):
    pin=tspath
    pout=os.path.join(tmppath,"chscan.txt")
    chopt="-ch"
    if ch.find("CS")>-1 or ch.find("BS")>-1 :
        chopt="-cs"
    elif int(ch)>99:
        chopt="-bs"
    doexe = tssplitterp + " "+chopt+" \""+ pin + "\" \""+ pout + "\""
    doexe = "nice -n 18 " + doexe
    os.environ['LANG']="ja_JP.UTF-8"
    os.system(u"touch \""+pout+"\"")
    txt = unicode(commands.getoutput(doexe.encode('utf-8')),'utf-8','ignore')
    f=open(pout)
    lines=f.readlines()
    f.close()
    ret=[]
    for l in lines:
        try:
            l=l.replace(" ","")
            l=l.replace("\n","")
            ret.append(str(int(l)))
        except:
            ""
    recdblist.addLog(pin, doexe, u"TsSplitログ-コマンド")
    recdblist.addLog(pin, txt, u"TsSplitログ-詳細")
    time.sleep(1)
    try:
        ""
        os.remove(pout)
    except:
        ""
    return ret

def getB25UnparchasedList(logtxt):
    warning = 0
    ret=[]
    for strt in logtxt.splitlines():
        if strt.find("unpurchased ECM")>-1:
            warning=1
        if strt.find("total TS packet")>-1 and warning>0:
            warning=0
        if strt.find("channel")>-1 and warning>0:
            txt=strt.replace("channel:","")
            txt=txt.replace("\n","")
            txt=txt.replace(" ","")
            ret.append(txt)
    return ret

def checkB25Purchased(ch,logtxt):
    chlists=chdb.searchAllCHFromCh(ch)
    upl=getB25UnparchasedList(logtxt)
    if len(upl)>0 and len(chlists)>0:
        for chl in chlists:
            if chl != None:
                if chl['csch'] in upl:
                    chdb.changeCHShow(chl['chtxt'],"0")
if __name__ == "__main__":
    searchCh(forced=1)