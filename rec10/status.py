#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import configreader
import os
import rec10d

path = os.path.dirname(os.path.abspath(__file__)) + "/"

def changeEncoding(i):
    """
    iはint　増減
    """
    rec10d.rec10db.change_ts2avi_in_status(i)
def changeTERecording(i):
    rec10d.rec10db.change_terec_in_status(i)
def changeBSCSRecording(i):
    rec10d.rec10db.change_bscsrec_in_status(i)
def changeB25Decoding(i):
    rec10d.rec10db.change_b252ts_in_status(i)
def getEncoding():
    """
    エンコードしている数を帰すint型
    """
    return int(rec10d.rec10db.select_all_in_status()[0][0])
def getTERecording():
    return int(rec10d.rec10db.select_all_in_status()[0][1])
def getBSCSRecording():
    return int(rec10d.rec10db.select_all_in_status()[0][2])
def getB25Decoding():
    return int(rec10d.rec10db.select_all_in_status()[0][3])
def getSettings_auto_bayes():
    return int(rec10d.rec10db.select_all_in_settings()[0][1])
def getSettings_auto_jbk():
    return int(rec10d.rec10db.select_all_in_settings()[0][0])
def getSettings_auto_del_tmp():
    return int(rec10d.rec10db.select_all_in_settings()[0][2])
def getSettings_auto_opt():
    return rec10d.rec10db.select_all_in_settings()[0][3]

def getRecordingMax():
    return [int(configreader.getConfEnv("te_max")),int(configreader.getConfEnv("bscs_max"))]
