#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import commands
import configreader
import os
import os.path
import sys
import re
import random
import time
import traceback
import zip

import recdblist

def ts2x264(pin, pout, opts):#sizeは"HD"か"SD"
    """
    pinで指定されたファイルをpoutにx264でエンコードして書き出す
    """
    is24fps=0
    size="HD"
    crf=18
    quality=4
    quality=int(configreader.getConfEnv("x264_preset"))
    crf=int(configreader.getConfEnv("crf"))
    deinterlace=1
    sar=1
    bluray=0
    try:
        bluray=int(configreader.getConfEnv("bluray"))
    except:
        bluray=0
    
    if re.search("H", opts):
        size = "HD"
    if re.search("S", opts):
        size = "WVGA"
    if re.search("F", opts):
        size = "FullHD"
    if re.search("W",opts):
        size = "WVGA"
    if re.search("v", opts):
        is24fps=1
        crf=int(configreader.getConfEnv("animation_crf"))
    if re.search("a", opts):
        is24fps=1
        crf=int(configreader.getConfEnv("animation_crf"))
    if re.search("I", opts):
        deinterlace=0
    if re.search("B", opts):
        bluray=1
    if bluray == 1:
        sar=0
    if re.search("q",opts):
        quality=quality-2
    if re.search("w",opts):
        quality=quality-1
    if re.search("e",opts):
        quality=quality+1
    if re.search("r",opts):
        quality=quality+2
    if re.search("u",opts):
        crf=crf+2
    if re.search("i",opts):
        crf=crf+1
    if re.search("o",opts):
        crf=crf-1
    if re.search("p",opts):
        crf=crf-2
    if re.search("d",opts) or re.search("s",opts):#二カ国語放送の場合
        tm2v=pin.replace(".ts",".m2v")
        encodeFfmpegSar(tm2v,pout,size,is24fps,quality,crf,deinterlace=deinterlace,usebluray=bluray,usesar=sar)
    elif re.search("5",opts):#5.1chの場合
        encodeFfmpegSar(pin,pout,size,is24fps,quality,crf,deinterlace=deinterlace,usebluray=bluray,usesar=sar)
    elif re.search("b",opts):#BonTsDemuxを使いたい場合
        tm2v=pin.replace(".ts",".m2v")
        encodeFfmpegSar(tm2v,pout,size,is24fps,quality,crf,deinterlace=deinterlace,usebluray=bluray,usesar=sar)
    else:
        try:
            encodeMencoderSar(pin, pout,size,is24fps,quality,crf,deinterlace=deinterlace,uselavf=1,usebluray=bluray,usesar=sar)
            if os.path.exists(pout):
                if not os.path.getsize(pout)>1*1000:
                    recdblist.addCommonlogEX("Warning", "Old mencoder option used.","","",log_level=200)
                    try:
                        encodeMencoderSar(pin, pout,size,is24fps,quality,crf,deinterlace=deinterlace,uselavf=0,usebluray=bluray,usesar=sar)
                    except Exception, inst:
                        recdblist.addCommonlogEX("Error", "ts2x264_mencoder_old(ts2x264.py)", str(type(inst)), str(inst)+traceback.format_exc(),log_level=200)
            else:
                recdblist.addCommonlogEX("Warning", "Old mencoder option used.","","",log_level=200)
                try:
                    encodeMencoderSar(pin, pout,size,is24fps,quality,crf,deinterlace=deinterlace,uselavf=0,usebluray=bluray,usesar=sar)
                except Exception, inst:
                    recdblist.addCommonlogEX("Error", "ts2x264_mencoder_old(ts2x264.py)", str(type(inst)), str(inst)+traceback.format_exc(),log_level=200)
        except Exception, inst:
            recdblist.addCommonlogEX("Error", "ts2x264(ts2x264.py)", str(type(inst)), str(inst)+traceback.format_exc(),log_level=200)

def encodeMencoderSar(pin,pout,size,is24fps,quality,crf,deinterlace=1,uselavf=0,usebluray=0,usesar=1):
    mencoder=configreader.getConfPath("mencoder")
    tee=configreader.getConfPath("tee")
    encvf=""
    txt=""
    encvf="-sws 9 -vf yadif=0,pp=l5"
    harddup=",hqdn3d=2:1:2,unsharp=l3x3:0.75:c3x3:0.75,harddup"
    ofps="-ofps 30000/1001"
    fps="-fps 30000/1001"
    x264fps="30000/1001 --keyint 30"
    x264streamsize=""
    x264preset=""
    x264tune=""
    x264_bitrate="5000"
    x264interlaced=0
    x264sar="1:1"
    tsar=getMoviePAR2(pin)
    if usesar==1:
        x264sar=str(tsar[0])+u":"+str(tsar[1])
    lavft=""
    if is24fps==1:
        ofps="-ofps 24000/1001"
        fps="-fps 30000/1001"
        x264fps="24000/1001 --keyint 24"
        x264tune="--tune animation"
        encvf="-sws 9 -vf pullup,softskip"
        harddup=",pp=l5,unsharp=l3x3:0.75:c3x3:0.75,hqdn3d=2:1:2,harddup"
    if size == "HD":
        tsize=getParSize(pin,720)
        if usesar==1:
            encvf = encvf + ",scale=-3:720::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x720"
        else:
            encvf = encvf + ",scale=-2:720::0:3,expand=1280:720"+harddup
            x264streamsize=u"1280x720"
        x264_bitrate="2500"
    elif size == "WVGA":
        tsize=getParSize(pin,480)
        if usesar==1:
            encvf = encvf + ",scale=-3:480::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x480"
        else:
            encvf = encvf + ",scale=-2:480::0:3,expand=854:480"+harddup
            x264streamsize=u"854x480"
        x264_bitrate="1500"
    elif size == "FullHD":
        tsize=getParSize(pin,1080)
        if usesar==1:
            encvf = encvf + ",scale=-3:1080::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x1080"
        else:
            encvf = encvf + ",scale=-2:1080::0:3,expand=1920:1080"+harddup
            x264streamsize=u"1920x1080"
        x264_bitrate="5000"
    elif size == "QVGA_BASE":
        tsize=getParSize(pin,240)
        if usesar==1:
            encvf = encvf + ",scale=-3:240::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x240"
        else:
            encvf = encvf + ",scale=320:-2::0:3,expand=320:240"+harddup
            x264streamsize=u"320x240"
        x264_bitrate="300"
    elif size == "WVGA_BASE":
        tsize=getParSize(pin,480)
        if usesar==1:
            encvf = encvf + ",scale=-3:480::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x480"
        else:
            encvf = encvf + ",scale=-2:480::0:3,expand=854:480"+harddup
            x264streamsize=u"854x480"
        x264_bitrate="1500"
    else:
        tsize=getParSize(pin,720)
        if usesar==1:
            encvf = encvf + ",scale=-3:720::0:3"+harddup
            x264streamsize=str(tsize[0])+u"x720"
        else:
            encvf = encvf + ",scale=-2:720::0:3,expand=1280:720"+harddup
            x264streamsize=u"1280x720"
        x264_bitrate="2500"
    if deinterlace==0:
        tsize=getMovieBaseSize2(pin)
        ofps="-ofps 30000/1001"
        #fps="-fps 30000/1001"
        fps=""
        x264fps="30000/1001 --keyint 30"
        x264interlaced=1
        encvf="-vf hqdn3d=2:1:2"
        harddup=",harddup"
        encvf=encvf+harddup
        x264streamsize=str(tsize[0])+u"x"+str(tsize[1])
        if usebluray==1:
            encvf = u"-sws 9 "+encvf + ",scale=-2:1080::0:3,expand=1920:1080"+harddup
            x264streamsize=u"1920x1080"
    if tsize[0] <= 0 or tsize[1] <= 0:
        encvf="-sws 9 -vf yadif=0,pp=l5"
        harddup=",hqdn3d=2:1:2,unsharp=l3x3:0.75:c3x3:0.75,harddup"
        ofps="-ofps 30000/1001"
        fps="-fps 30000/1001"
        x264fps="30000/1001 --keyint 30"
        tsize=[1280,720]
        encvf = encvf + ",scale=-2:720::0:3,expand=1280:720"+harddup
        x264streamsize=u"1280x720"
        x264_bitrate="2500"
    if quality==1:
        x264preset=u"ultrafast"
    elif quality==2:
        x264preset=u"veryfast"
    elif quality==3:
        x264preset=u"fast"
    elif quality==4:
        x264preset=u"medium"
    elif quality==5:
        x264preset=u"slow"
    elif quality==6:
        x264preset=u"slower"
    else:
        x264preset=u"medium"
    if size == "WVGA_BASE" or size == "QVGA_BASE":
        x264profile=" --level 32 --profile baseline "
    else:
        x264profile=" --level 41 --profile high "
    if uselavf==1:
        lavft="-demuxer lavf "
    x264crf=str(crf)
    os.environ['LANG']="ja_JP.UTF-8"
    random.seed(pin)
    random.jumpahead(10)
    temptime=int(time.time())
    temptime=temptime % 9697
    random.jumpahead(temptime)
    streampath=os.path.join(os.path.dirname(pin),str(random.randint(10000, 99999999)))
    unicode(commands.getoutput(u"mkfifo "+streampath),'utf-8','ignore')
    logmencoder=recdblist.getLogTitle(pin)+".mencoder.log"
    logx264=recdblist.getLogTitle(pin)+".x264.log"
    encexe=mencoder+u" \""+pin+u"\" -quiet -vfm ffmpeg "+lavft+encvf+u",format=i420 "+fps+" "+ofps+" -oac mp3lame -ovc raw -of rawvideo -o \""+streampath+"\" 2>&1 | "+tee+" \""+logmencoder+"\" & "
    encexe=encexe+getX264Commandline(x264preset, x264sar, x264fps, x264profile, x264tune, pout, streampath, x264streamsize, crf=x264crf,interlaced=x264interlaced,bluray=usebluray)
    #encexe=encexe+u" nice -n 19 "+x264+" "+x264_sar+" "+x264crf+u" "+x264_addline+u"  --threads "+x264_thread+" "+x264profile+x264preset+" "+x264tune+" "+x264fps+" -o \""+pout+"\" "+streampath+" "+x264streamsize
    encexe=u"nice -n 19 " +encexe+" 2>&1 | "+tee+" \""+logx264+"\""
    try:
        recdblist.addCommandSelfLog(pin, encexe)
    except Exception, inst:
        print type(inst)
        print str(inst)
        print traceback.print_exc(file=sys.stdout)
    recdblist.printutf8(encexe)
    txt=""
    try:
        txt=unicode(commands.getoutput(encexe.encode('utf-8')),'utf-8','ignore')
    except:
        ""
    os.remove(streampath)
    zip.addFile2FileZip(logmencoder, recdblist.getLogTitle(pin)+".log.zip")
    zip.addFile2FileZip(logx264, recdblist.getLogTitle(pin)+".log.zip")
    os.remove(logmencoder)
    os.remove(logx264)
    recdblist.addCommandLog(pin, u"Mencoder", encexe, txt)
#    recdblist.addCommandLogTar(pin,u"Mencoder","mencoder", encexe,txt)

def encodeFfmpegSar(pin,pout,size,is24fps,quality,crf,deinterlace=1,usebluray=0,usesar=1):
    """

    """
    ffmpeg=configreader.getConfPath("ffmpeg")
    tee=configreader.getConfPath("tee")
    fps=u"-r 29.970030 "
    x264fps="30000/1001 --keyint 30"
    x264streamsize=""
    x264preset=""
    x264tune=""
    x264_bitrate="2500"
    x264_thread="auto"
    x264interlaced=0
    tsar=getMoviePAR2(pin)
    filter="-deinterlace"
    x264sar=str(tsar[0])+":"+str(tsar[1])
    if is24fps==1:
        fps=u"-r 23.976023 "
        x264fps="24000/1001 --keyint 24"
        x264tune="--tune animation"
    if size == "HD":
        tsize=getParSize(pin,720)
        s = "-s "+str(tsize[0])+"x720 "
        x264streamsize=str(tsize[0])+u"x720"
        x264_bitrate="2500"
    elif size == "WVGA":
        tsize=getParSize(pin,480)
        s = "-s "+str(tsize[0])+"x480 "
        x264streamsize=str(tsize[0])+u"x480"
        x264_bitrate="1500"
    elif size == "FullHD":
        tsize=getParSize(pin,1080)
        s = "-s "+str(tsize[0])+"x1080 "
        x264streamsize=str(tsize[0])+u"x1080"
        x264_bitrate="5000"
    elif size == "SD":
        tsize=getParSize(pin,480)
        s = "-s "+str(tsize[0])+"x480 "
        x264streamsize=str(tsize[0])+u"x480"
        x264_bitrate="1250"
    elif size == "QVGA_BASE":
        tsize=getParSize(pin,240)
        s = "-s "+str(tsize[0])+"x240 "
        x264streamsize=str(tsize[0])+u"x240"
        x264_bitrate="300"
    elif size == "WVGA_BASE":
        tsize=getParSize(pin,480)
        s = "-s "+str(tsize[0])+"x480 "
        x264streamsize=str(tsize[0])+u"x480"
        x264_bitrate="1500"
    else:
        tsize=getParSize(pin,720)
        s = "-s "+str(tsize[0])+"x720 "
        x264streamsize=str(tsize[0])+u"x720"
        x264_bitrate="2500"
    if deinterlace==0:
        tsize=getMovieBaseSize2(pin)
        fps=""
        s = "-s "+str(tsize[0])+"x"+str(tsize[1])+" "
        x264fps="30000/1001 --keyint 30"
        x264interlaced=1
        filter=""
        x264streamsize=str(tsize[0])+u"x"+str(tsize[1])
    if quality==1:
        x264preset=u"ultrafast"
    elif quality==2:
        x264preset=u"veryfast"
    elif quality==3:
        x264preset=u"fast"
    elif quality==4:
        x264preset=u"medium"
    elif quality==5:
        x264preset=u"slow"
    elif quality==6:
        x264preset=u"slower"
    else:
        x264preset=u"medium"
    if size == "WVGA_BASE" or size == "QVGA_BASE":
        x264profile=" --level 32 --profile baseline "
    else:
        x264profile=" --level 41 --profile high "
    x264crf=str(crf)
    txt=""
    os.environ['LANG']="ja_JP.UTF-8"
    log_ffmpeg=recdblist.getLogTitle(pin)+".ffmpeg.log"
    log_x264=recdblist.getLogTitle(pin)+".x264.log"
    exe=ffmpeg+u" -y -i \""+pin+"\" -vcodec rawvideo -pix_fmt yuv420p "+s+fps+" "+filter+" -an -f rawvideo - 2> \""+log_ffmpeg+"\" | "
    exe=exe+getX264Commandline(x264preset, x264sar, x264fps, x264profile, x264tune, pout,"-", x264streamsize, crf=x264crf,interlaced=x264interlaced,bluray=usebluray)
    exe = "nice -n 19 " + exe+" 2>&1 | "+tee+" \""+log_x264+"\""
    txt=""
    recdblist.printutf8(exe)
    recdblist.addCommandSelfLog(pin,exe)
    try:
        txt=unicode(commands.getoutput(exe.encode('utf-8')),'utf-8','ignore')
    except:
        ""
    recdblist.addCommandLog(pin, u"FFmpeg動画エンコード", exe, txt)
    zip.addFile2FileZip(log_ffmpeg, recdblist.getLogTitle(pin+".log.zip"))
    zip.addFile2FileZip(log_x264, recdblist.getLogTitle(pin+".log.zip"))
    os.remove(log_ffmpeg)
    os.remove(log_x264)
    
def getMovieBaseSize2(pin):##動画開始後すぐに解像度が変更されたときに対処
    ffmpeg=configreader.getConfPath("ffmpeg")
    os.environ['LANG']="ja_JP.UTF-8"
    exe=ffmpeg+u" -y -ss 5 -t 10 -i \""+pin+"\" \""+pin+".size.ts\" 2>&1"
    try:
        recdblist.addCommandSelfLog(pin, exe)
    except Exception, inst:
        print type(inst)
        print str(inst)
        print traceback.print_exc(file=sys.stdout)
    txts=unicode(commands.getoutput(exe.encode('utf-8')),'utf-8','ignore')
    rT=re.compile(u"Stream.*#.*:.*\D+([\d]+x[\d]+)\D+PAR\D+(\d+:\d+)\D+.*\Z")
    rT2=re.compile(u".*Stream.*#.*:.*\D+([\d]+x[\d]+)\D+SAR\D+(\d+:\d+)\D+.*DAR\D+(\d+:\d+)\D+.*\Z")
    sizeMaxX=0
    sizeMaxY=0
    txtls=txts.split("\n")
    for t in txtls:
        rM=rT.search(t)
        rM2=rT2.search(t)
        if rM:
            sizetxt=rM.group(1)
            partxt=rM.group(2)
            tX=int(sizetxt.split("x")[0])
            tY=int(sizetxt.split("x")[1])
            tEX=int(partxt.split(":")[0])
            tEY=int(partxt.split(":")[1])
            if sizeMaxX<tX:
                sizeMaxX=tX
                sizeMaxY=tY
        if rM2:
            sizetxt=rM2.group(1)
            tX=int(sizetxt.split("x")[0])
            tY=int(sizetxt.split("x")[1])
            if sizeMaxX<tX:
                sizeMaxX=tX
                sizeMaxY=tY
    os.remove(pin+".size.ts")
    return [sizeMaxX,sizeMaxY]

def getMoviePAR2(pin):
    ffmpeg=configreader.getConfPath("ffmpeg")
    os.environ['LANG']="ja_JP.UTF-8"
    exe=ffmpeg+u" -ss 5 -t 10 -i \""+pin+"\" \""+pin+".size.ts\" 2>&1"
    try:
        recdblist.addCommandSelfLog(pin, exe)
    except Exception, inst:
        print type(inst)
        print str(inst)
        print traceback.print_exc(file=sys.stdout)
    txts = unicode(commands.getoutput(exe.encode('utf-8')), 'utf-8', 'ignore')
    rT = re.compile(u".*Stream.*#.*:.*\D+([\d]+x[\d]+)\D+PAR\D+(\d+:\d+)\D+.*\Z")
    rT2 = re.compile(u".*Stream.*#.*:.*\D+([\d]+x[\d]+)\D+SAR\D+(\d+:\d+)\D+.*DAR\D+(\d+:\d+)\D+.*\Z")
    sizeMaxX = 0
    parx = 0
    pary = 0
    txtls = txts.split("\n")
    for t in txtls:
        rM = rT.match(t)
        rM2 = rT2.match(t)
        if rM:
            sizetxt = rM.group(1)
            dartxt = rM.group(2)
            tX = int(sizetxt.split("x")[0])
            tY = int(sizetxt.split("x")[1])
            tEX = int(dartxt.split(":")[0])
            tEY = int(dartxt.split(":")[1])
            if sizeMaxX < tX:
                sizeMaxX = tX
                if tX == 1920 and tY == 1080:
                    parx = 1
                    pary = 1
                else:
                    parx = tEX
                    pary = tEY
        rM = rT.match(t)
        if rM2:
            sizetxt = rM2.group(1)
            sartxt = rM2.group(2)
            dartxt = rM2.group(3)
            tX = int(sizetxt.split("x")[0])
            tY = int(sizetxt.split("x")[1])
            tEX = int(dartxt.split(":")[0])
            tEY = int(dartxt.split(":")[1])##dar=sarxpar
            tSX = int(sartxt.split(":")[0])
            tSY = int(sartxt.split(":")[1])##par=dar/sar= darx*pary*sary=dary*parx*sarx
            if sizeMaxX < tX:
                sizeMaxX = tX
                if tX == 1920 and tY == 1080:
                    parx = 1
                    pary = 1
                else:
                    if tEX * tSY == tEY * tSX:
                        parx = 1
                        pary = 1
                    elif tEX * tSY * 3 == tEY * tSX * 4:
                        parx = 4
                        pary = 3
                    elif tEX * tSY * 4 == tEY * tSX * 3:
                        parx = 3
                        pary = 4
                    elif tEX * tSY * 9 == tEY * tSX * 16:
                        parx = 16
                        pary = 9
                    elif tEX * tSY * 16 == tEY * tSX * 9:
                        parx = 9
                        pary = 16
    os.remove(pin+".size.ts")
    return [parx,pary]

def getParSize(pin,y):
    tSize=getMovieBaseSize2(pin)
    if tSize[1] != 0 :
        tX=tSize[0]*10*y/tSize[1]
        tY=y
        if tX>int(tX/10)*10:
            tX=tX/10+1
        else:
            tX=tX/10
    else:
        tX=-1
        tY=-1
    return [tX,tY]

def getX264CoreVersion():
    x264=configreader.getConfPath("x264")
    t1=commands.getoutput(x264+" --help|grep core")
    rT=re.compile(u"x264 core:(\d*)[\d]*.*\Z")
    rM=rT.match(t1)
    v=-1
    if rM:
        v=int(rM.group(1))
    return v

def getX264Commandline(preset,sar,fps,x264profile,x264tune,pout,pin,x264streamsize,crf=-1,bitrate=0,interlaced=0,bluray=0):
    x264=configreader.getConfPath("x264")
    os.environ['LANG']="ja_JP.UTF-8"
    x264_sar="--sar "+sar
    x264preset=u"--preset "+preset
    x264fps="--fps "+fps
    x264interlaced=""
    x264bluray=""
    x264_addline=configreader.getConfEnv("x264_addline")
    if crf==-1:
        x264bitrate=u"--bitrate "+str(bitrate)
    else:
        x264crf=u"--crf "+str(crf)
    if interlaced==1:
        x264interlaced="--tff --nal-hrd vbr"
    if bluray==1:
        if getX264CoreVersion()<115:
            x264bluray=" --weightp 1 --nal-hrd vbr --bframes 3 --b-pyramid none --open-gop bluray --slices 4 --aud --colorprim bt709 --transfer bt709 "
            x264_addline=""
            if fps=="24000/1001 --keyint 24" or fps =="24000/1001":
                if x264streamsize=="1280x720":
                    x264bluray=u"--ref 6 "+x264bluray
                if x264streamsize=="1920x1080":
                    x264bluray=u"--ref 4 "+x264bluray
            elif fps=="30000/1001 --keyint 30" or fps =="30000/1001":
                if x264streamsize=="1280x720":
                    x264bluray=u"--ref 6 --pulldown double "+x264bluray
                if x264streamsize=="1920x1080":
                    if interlaced==0:
                        x264bluray=u"--ref 4 --fake-interlaced --pic-struct "+x264bluray
                    else:
                        x264bluray=u"--ref 4 "+x264bluray
        elif getX264CoreVersion()>=115:
            x264bluray=" --slices 4 --bluray-compat --nal-hrd vbr --bframes 3 --b-pyramid strict --aud --colorprim bt709 --transfer bt709 "
            x264_addline=""
            if fps=="24000/1001 --keyint 24" or fps =="24000/1001":
                if x264streamsize=="1280x720":
                    x264bluray=u"--ref 6 "+x264bluray
                if x264streamsize=="1920x1080":
                    x264bluray=u"--ref 4 "+x264bluray
            elif fps=="30000/1001 --keyint 30" or fps =="30000/1001":
                if x264streamsize=="1280x720":
                    x264bluray=u"--ref 6 --pulldown double "+x264bluray
                if x264streamsize=="1920x1080":
                    if interlaced==0:
                        x264bluray=u"--ref 4 --fake-interlaced --pic-struct "+x264bluray
                    else:
                        x264bluray=u"--ref 4 "+x264bluray
    x264_thread="auto"
    try:
        xtt=configreader.getConfEnv("x264_thread")
        xtt=int(xtt)
        if xtt>0:
            x264_thread=str(xtt)
    except:
        x264_thread="auto"
    x264_addline=configreader.getConfEnv("x264_addline")
    if getX264CoreVersion()>103:
        x264res=u"--input-res "+x264streamsize
        exe=u"nice -n 19 "+x264+" --demuxer raw --vbv-maxrate 30000 --vbv-bufsize 25000 "+x264_sar+" "+x264crf+u" "+x264bluray+u" "+x264interlaced+u" "+x264_addline+u" --colormatrix bt709 --threads "+x264_thread+" "+x264profile+x264preset+" "+x264tune+" "+x264fps+" "+x264res+" -o \""+pout+"\" "+pin
    else:
        exe=u"nice -n 19 "+x264+" --demuxer raw --vbv-maxrate 30000 --vbv-bufsize 25000 "+x264_sar+" "+x264crf+u" "+x264bluray+u" "+x264interlaced+u" "+x264_addline+u" --colormatrix bt709 --threads "+x264_thread+" "+x264profile+x264preset+" "+x264tune+" "+x264fps+" -o \""+pout+"\" "+pin+" "+x264streamsize
    return exe
