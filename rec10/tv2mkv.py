#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import os
import os.path
import tv2mp4
import configreader
import subprocess

def ts2mkv(pin, pout, opt):
    tpout=pout.replace(".mkv",".mp4")
    tv2mp4.ts2mp4(pin, tpout, opt)
    mp42mkv(pout, tpout)
    if os.path.exists(pout) and os.path.getsize(pout)>os.path.getsize(tpout)*0.9:
        os.remove(tpout)
        
def mp42mkv(pmkv,pmp4):
    exe = configreader.getConfPath("mkvmerge")
    e1=exe +" -o \""+pmkv+u"\" \""+pmp4+"\""
    p=subprocess.Popen(e1,shell=True)
    os.waitpid(p.pid, 0)
