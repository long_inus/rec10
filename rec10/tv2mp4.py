#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze
#

import commands
import shutil
import auto_process
import os
import re
import os.path
import string
import base64
import time
import subprocess
import traceback
import zip

import tv2avi
import recdblist
import configreader
import status
import tv2audio
import rec10errors

path = str(os.path.dirname(os.path.abspath(__file__))) + "/"
tmppath = configreader.getConfPath("tmp") + "/"

if tmppath == "/":
    tmppath = path
if not os.path.exists(tmppath):
    os.mkdir(tmppath)


def ts2mp4(pin, pout, opt, skipVideoEncode=0):
    useLSmash = int(configreader.getConfPath("useLSmash"))
    dir = os.path.split(pout)[0]
    title = os.path.split(pout)[1]
    title = os.path.splitext(title)[0]
    tpraw = os.path.join(dir, title + ".264")
    tpmp4 = os.path.join(dir, title + ".mp4")
    tpm4a = os.path.join(dir, title + ".m4a")
    tpm4v = os.path.join(dir, title + ".m4v")
    tpaac = os.path.join(dir, title + ".aac")
    if os.path.isfile(pin) and os.path.getsize(pin) > 10 * 1000:
        if useLSmash > 0:
            try:
                if skipVideoEncode == 0:
                    tv2avi.ts2m4v(pin, tpmp4, opt)
                addAudio(pin, tpmp4, pout, opt, useLsmash=1)
            except rec10errors.AudioCorruptedError, msg:
                try:
                    if os.path.exists(pout):
                        os.remove(pout)
                    addAudio(pin, tpm4v, pout, opt, useLsmash=0)
                except:
                    recdblist.addLog(pin, u"audioファイルが壊れています、BonTsDemuxを使用して再エンコードを行います。", "audio mux error")
                    if os.path.exists(tpaac):
                        os.remove(tpaac)
                    if os.path.exists(tpm4a):
                        os.remove(tpm4a)
                    if os.path.exists(tpm4v):
                        os.remove(tpm4v)
                    if os.path.exists(tpmp4):
                        os.remove(tpmp4)
                    time.sleep(5)
                    opt += "b"
                    tv2audio.ts2single_fp_BonTsDemux(pin)
                    tv2avi.ts2m4v(pin, tpmp4, opt)  # BonTsDemuxを使って動画をsplit,再エンコードをする
                    addAudio(pin, tpmp4, pout, opt, useLsmash=1)
        else:
            if skipVideoEncode == 0:
                tv2avi.ts2raw(pin, tpraw, opt)
            time.sleep(10)
            if os.path.isfile(tpraw) and os.path.getsize(tpraw) > 10 * 1000:
                raw2mp4(tpraw, tpmp4, opt)
            time.sleep(10)
            if os.path.exists(tpraw) and not re.search("B", opt):
                os.remove(tpraw)
    zip.addFile2FileZip(recdblist.getLogTitle(pin) + ".command.log", recdblist.getLogTitle(pin) + ".log.zip")
    if os.path.exists(recdblist.getLogTitle(pin) + ".command.log"):
        os.remove(recdblist.getLogTitle(pin) + ".command.log")
    dir = os.path.split(pout)[0]
    title = os.path.splitext(os.path.split(pout)[1])[0]
    if status.getSettings_auto_del_tmp() == 1:
        if os.path.exists(pout):
            if re.search("t", opt):
                auto_process.deleteTmpFile(dir, title, ".264")
            elif re.search("k", opt):
                ""  # 削除しない
            else:
                auto_process.deleteTmpFile(dir, title, ".mp4")


def raw2mp4(p_in_video, p_out_video, opt):
    newMP4withVideo_MP4Box(p_in_video, p_out_video, opt)
    p_in_video_ts = p_in_video.replace(".264", ".ts")
    addAudio(p_in_video_ts, p_out_video, opt)
    addCaption(p_in_video_ts, p_out_video)


def addCaption(pts, pmp4):  ##字幕の追加を試みる。
    wineexe = configreader.getConfPath("wine")
    pincap = pts.replace(".ts", ".srt")
    try:
        cap2ass = configreader.getConfPath("caption2ass")
    except:
        cap2ass = ""
    if os.path.isfile(cap2ass):
        e0 = u'%(wineexe)s %(cap2ass)s -format srt "Z:\\%(pts)s" "Z:\\%(pincap)s"' % locals()
        recdblist.printutf8(e0)
        p0 = subprocess.Popen(e0, shell=True, stdout=subprocess.PIPE)
        time.sleep(100)
        if p0.poll == None:  # 実行中
            os.waitpid(p0.pid, 0)
            logt = unicode(p0.communicate()[0], "UTF-8")
            recdblist.addLog(pts, e0, u"Captionログ-コマンド")
            recdblist.addLog(pts, logt, u"Captionログ-詳細")
            recdblist.addCommandLogZip(pts, "mp4box_caption", "mp4box_caption", e0, logt)
        if os.path.exists(pincap):
            if os.path.getsize(pincap) > 1000:
                exe = configreader.getConfPath("mp4box") + u" -tmp " + tmppath
                e1s = u'%(exe)s -add "%(pincap)s" "%(pmp4)s"' % locals()
                addmp4(pincap, pmp4, e1s)


def addAudio(pts, p_input_video, p_output_all, opts, useLsmash=0):  # オプションに応じた音声の追加を行う
    pmp4 = p_output_all
    pts = pts.replace(".m4v", ".ts")
    pts = pts.replace(".m2v", ".ts")
    paac = pts.replace(".ts", ".aac")
    if re.search("d", opts) or re.search("5", opts) or re.search("s", opts):  # 二カ国語放送/5.1ch放送の場合
        paac1 = pts.replace(".ts", "_1.aac")
        paac2 = pts.replace(".ts", "_2.aac")
        recdblist.printutf8(paac1)
        if not os.path.exists(paac1):
            paac1 = pts.replace(".ts", "_1.mp3")
        if not os.path.exists(paac2):
            paac2 = pts.replace(".ts", "_2.mp3")
        if os.path.exists(paac1) and os.path.exists(paac2):
            if useLsmash == 1:
                addMultiAudio_lsmash(p_input_video, paac1, paac2, pmp4)
            else:
                addSingleAudio_MP4Box(p_input_video, paac1, pmp4)
                addSingleAudio_MP4Box(p_input_video, paac2, pmp4)
    elif re.search("b", opts):  # BonTsDemuxを使って音声をスプリットした場合
        if not os.path.exists(paac):
            paac = pts.replace(".ts", ".mp3")
        if os.path.exists(paac):
            if useLsmash == 1:
                addSingleAudio_lsmash(p_input_video, paac, pmp4)
            else:
                addSingleAudio_MP4Box(p_input_video, paac, pmp4)
    else:
        if not os.path.exists(paac):
            tv2audio.ts2single_audio(pts, opts)
        if not os.path.exists(paac):
            paac = pts.replace(".ts", ".mp3")
        if os.path.exists(paac):
            if useLsmash == 1:
                addSingleAudio_lsmash(p_input_video, paac, pmp4)
            else:
                addSingleAudio_MP4Box(p_input_video, paac, pmp4)


def execmp4box(pin, pout, cmd):
    title = os.path.splitext(os.path.split(pin)[1])[0]
    nt = base64.b16encode(title.encode('utf-8'))
    if len(nt) > 200:
        nt = nt[:180]
    ptin = os.path.join(os.path.dirname(pin), nt + ".264")
    recdblist.printutf8(ptin)
    shutil.move(pin, ptin)
    time.sleep(10)
    ptout = os.path.join(os.path.dirname(pout), nt + ".mp4")
    cmdn = string.replace(cmd, pin, ptin)
    cmdn = string.replace(cmdn, pout, ptout)
    recdblist.printutf8(cmdn)
    recdblist.addCommandSelfLog(pin, cmdn)
    recdblist.execCommand(cmdn, pout, "execMP4Box", "execMP4Box")
    time.sleep(5)
    shutil.move(ptin, pin)
    shutil.move(ptout, pout)
    time.sleep(5)


def addmp4(padd, pout, cmd):  # without video  use MP4Box
    title = os.path.splitext(os.path.split(padd)[1])[0]
    ext = os.path.splitext(os.path.split(padd)[1])[1]
    nt = base64.b16encode(title.encode('utf-8'))
    if len(nt) > 200:
        nt = nt[:180]
    ptadd = os.path.join(os.path.dirname(padd), nt + ext)
    ptoutb = os.path.join(os.path.dirname(pout), nt + "_b.mp4")
    ptout = os.path.join(os.path.dirname(pout), nt + ".mp4")
    shutil.move(padd, ptadd)
    if os.path.isfile(pout):
        shutil.move(pout, ptoutb)
    time.sleep(5)
    cmdn = string.replace(cmd, padd, ptadd)
    cmdn = string.replace(cmdn, u'-out "' + pout, '-add "' + ptoutb + u'" -new "' + ptout)
    cmdn = string.replace(cmdn, u'"' + pout, u'-add "' + ptoutb + '" -new "' + ptout)
    cmdn = string.replace(cmdn, pout, ptout)
    recdblist.printutf8(cmdn)
    recdblist.execCommand(cmdn, pout, "addmp4", "addmp4")
    time.sleep(5)
    shutil.move(ptadd, padd)
    if os.path.exists(ptout):
        shutil.move(ptout, pout)
        os.remove(ptoutb)
    else:
        txtt = padd + u"のインポートエラー"
        recdblist.addLog(pout, txtt, u"MP4Box追加ログ-コマンド")
        shutil.move(ptoutb, pout)
    time.sleep(5)


def newMP4withVideo_MP4Box(p_in_video, p_out_video, opt):
    duration = "-fps 29.970030 "
    if re.search("a", opt):
        duration = "-fps 23.976023 "
    if re.search("I", opt):
        duration = "-fps 29.970030 "
    exe = configreader.getConfPath("mp4box") + u" -tmp " + tmppath
    dirname = os.path.dirname(p_in_video)
    title = os.path.splitext(os.path.split(p_in_video)[1])[0]
    name_temp = base64.b16encode(title.encode('utf-8'))
    if len(name_temp) > 200:
        name_temp = name_temp[:180]
    p_temp_in_video = os.path.join(dirname, name_temp + ".264")
    recdblist.printutf8(p_temp_in_video)
    shutil.move(p_in_video, p_temp_in_video)
    time.sleep(10)
    p_temp_out_video = os.path.join(dirname, name_temp + ".mp4")
    cmdn = u'%(exe)s %(duration)s -add "%(p_temp_in_video)s" -new "%(p_temp_out_video)s"' % locals()
    recdblist.execCommand(cmdn, p_out_video, "newMP4withVideo_MP4Box", "newMP4withVideo_MP4Box")
    time.sleep(5)
    shutil.move(p_temp_in_video, p_in_video)
    shutil.move(p_temp_out_video, p_out_video)
    time.sleep(5)


def fileMuxedCheck(p_in_video, p_in_audio, p_out_all):
    filesize_video = os.path.getsize(p_in_video)
    filesize_audio = os.path.getsize(p_in_audio)
    thrshld_low = filesize_audio * 0.9 + filesize_video
    filesize_muxed = os.path.getsize(p_out_all)
    if filesize_muxed < thrshld_low:
        if filesize_muxed > filesize_video:
            return 0
        else:
            return 1
    else:
        return 1


def addSingleAudio_MP4Box(p_in_video, p_in_audio, p_out_all):
    filepath_root = os.path.splitext(p_in_video)[0]
    exe = configreader.getConfPath("mp4box") + u" -tmp " + tmppath
    title = os.path.splitext(os.path.split(p_in_audio)[1])[0]
    ext = os.path.splitext(os.path.split(p_in_audio)[1])[1]
    ext_video = os.path.splitext(os.path.split(p_in_video)[1])[1]
    foldername = os.path.dirname(p_in_audio)
    name_temp = base64.b16encode(title.encode('utf-8'))
    if len(name_temp) > 200:
        name_temp = name_temp[:180]
    p_temp_in_audio = os.path.join(foldername, name_temp + ext)
    p_temp_in_video = os.path.join(foldername, name_temp + "_b." + ext_video)
    p_temp_out_all = os.path.join(foldername, name_temp + ".mp4")
    shutil.move(p_in_audio, p_temp_in_audio)
    shutil.move(p_in_video, p_temp_in_video)
    if os.path.isfile(p_out_all):
        shutil.move(p_out_all, u'%(title)s.old.mp4' % locals())
    time.sleep(5)
    t_aac_string = ":mpeg4"
    if ext == "mp3":
        t_aac_string = ""
    cmdn = u'%(exe)s -add "%(p_temp_in_audio)s"%(t_aac_string)s -add "%(p_temp_in_video)s" -new "%(p_temp_out_all)s"' % locals()
    recdblist.execCommand(cmdn, p_out_all, "addSingleAudio_MP4Box", "addSingleAudio_MP4Box")
    time.sleep(5)
    if os.path.exists(p_temp_in_video):
        shutil.move(p_temp_in_video, p_in_video)
    if os.path.exists(p_temp_in_audio):
        shutil.move(p_temp_in_audio, p_in_audio)
    if os.path.exists(p_temp_out_all):
        shutil.move(p_temp_out_all, p_out_all)
    else:
        txtt = p_temp_in_audio + u"のインポートエラー"
        recdblist.addLog(p_out_all, txtt, u"MP4Box追加ログ-コマンド")
    time.sleep(5)
    if not fileMuxedCheck(p_in_video, p_in_audio, p_out_all):
        raise rec10errors.AudioCorruptedError(p_in_audio)


def addSingleAudio_lsmash(p_in_video, p_in_audio, p_out_all):
    filepath_root = os.path.splitext(p_in_video)[0]
    if p_in_video == p_out_all:
        shutil.move(p_in_video, filepath_root + ".m4v")
        time.sleep(5)
        p_in_video = filepath_root + ".m4v"
    muxer = configreader.getConfPath("lsmash")
    remuxer = configreader.getConfPath("lsmash").replace("muxer", "remuxer")
    p_in_audio_m4a = filepath_root + ".m4a"
    remuxAudio_lsmash(p_in_audio, p_in_audio_m4a)
    cmdtxt = u'%(remuxer)s -i "%(p_in_video)s" -i "%(p_in_audio_m4a)s" -o "%(p_out_all)s"' % locals()
    recdblist.execCommand(cmdtxt, p_out_all, "L-Smash audio mux", "l-smash audio mux")
    if not fileMuxedCheck(p_in_video, p_in_audio, p_out_all):
        raise rec10errors.AudioCorruptedError(p_in_audio)


def addMultiAudio_lsmash(p_in_video, p_in_audio1, p_in_audio2, p_out_all):
    filepath_root = os.path.splitext(p_in_video)[0]
    pm4v = filepath_root + ".m4v"
    remuxVideo_lsmash(p_in_video, pm4v)
    time.sleep(5)
    if os.path.exists(p_in_video):
        shutil.move(p_in_video, filepath_root + ".old.m4v")
    p_in_video = pm4v
    time.sleep(5)
    muxer = configreader.getConfPath("lsmash")
    remuxer = configreader.getConfPath("lsmash").replace("muxer", "remuxer")
    p_in_audio1_m4a = os.path.splitext(p_in_audio1)[0] + ".m4a"
    p_in_audio2_m4a = os.path.splitext(p_in_audio2)[0] + ".m4a"
    remuxAudio_lsmash(p_in_audio1, p_in_audio1_m4a)
    remuxAudio_lsmash(p_in_audio2, p_in_audio2_m4a)
    cmdtxt = u'%(remuxer)s -i "%(p_in_video)s" -i "%(p_in_audio1_m4a)s" -i "%(p_in_audio2_m4a)s" -o "%(p_out_all)s"' % locals()
    recdblist.execCommand(cmdtxt, p_out_all, "L-Smash audio video mux", "l-smash audio video mux")


def remuxAudio_lsmash(pin, pout):
    if os.path.exists(pout):
        os.remove(pout)
    muxer = configreader.getConfPath("lsmash").replace(u"muxer", u"remuxer")
    cmdtxt = u'%(muxer)s -i "%(pin)s" -o "%(pout)s"' % locals()
    recdblist.execCommand(cmdtxt, pout, "L-Smash audio mux", "l-smash audio mux")
    if not os.path.exists(pout):
        recdblist.execCommand(cmdtxt.replace("remuxer", "muxer"), pout, "L-Smash audio mux", "l-smash audio mux")
    if not os.path.exists(pout):
        tv2audio.aac2m4a_ffmpeg(pin, pout)


def remuxVideo_lsmash(pin, pout):
    remuxer = configreader.getConfPath("lsmash").replace(u"muxer", u"remuxer")
    cmdtxt = u'%(remuxer)s -i "%(pin)s" -o "%(pout)s"' % locals()
    recdblist.execCommand(cmdtxt, pout, "L-Smash video mux ", "l-smash video mux")
    if not os.path.exists(pout):
        recdblist.execCommand(cmdtxt.replace("remuxer", "muxer"), pout, "L-Smash video mux", "l-smash video mux")
    if not os.path.exists(pout):
        shutil.move(pin, pout)
