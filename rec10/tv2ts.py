#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

import commands
import os
import os.path
import time
import traceback

import configreader
import status
import recdblist
import recording_earth_pt1

mypath = str(os.path.dirname(os.path.abspath(__file__))) + "/"

def tv2tsmix(pout, ch, time,tsid="0",getlog=0):
    tv2b25ts(pout + ".b25", ch, time,tsid)
    txt=b252tsmix(pout + ".b25", pout)
    return txt

def tv2ts(pout, ch, csch, time,tsid):
    if ch.replace(" ","").replace("CS","").isdigit():
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(1)
        else:
            status.changeTERecording(1)
        try:
            tv2b25ts(pout + ".b25", ch, time,tsid)
        except Exception, inst:
            recdblist.addCommonlogEX(u"Error",u"tv2ts(tv2ts.py)", str(type(inst)),str(inst)+traceback.format_exc(),log_level=200)
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(-1)
        else:
            status.changeTERecording(-1)
    else:
         ch="0"
         csch="0"
    b252tsmix(pout + ".b25", pout + ".tsmix")
    tch="0"
    if csch!=u"0":
        tch=csch
    elif ch==u"101":
        tch="101"
    elif ch==u"102":
        tch="102"
    #else:
    #    tch=ch
    tsmix2ts(pout + ".tsmix", pout, tch)
    if os.access(pout, os.F_OK):
        try:
            #os.remove(path + "/" + t + ext)
            ""
        except:
            ""
            
def b252ts(pout, ch, csch):
    b252tsmix(pout + ".b25", pout + ".tsmix")
    tch=u"0"
    if ch.replace(" ","").replace("CS","").isdigit():
        if csch!=u"0":
            tch=csch
        elif ch==u"101":
            tch=u"101"
        elif ch==u"102":
            tch=u"102"
    else:
        tch="0"
    tsmix2ts(pout + ".tsmix", pout, tch)
    
def tv2b25ts(pout, ch, time,tsid):
    if recording_earth_pt1.useDVB()==0:
        exe = configreader.getConfPath('recpt1')
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(1)
        else:
            status.changeTERecording(1)
        try:
            doexe = exe + ' ' + ch + ' ' + time + ' \'' + pout + '\''
            recdblist.printutf8(doexe)
            recdblist.addLog(pout, doexe, u"recpt1ログ-コマンド")
            recdblist.addLog(pout, unicode(commands.getoutput(doexe.encode('utf-8')),'utf-8'), u"recpt1ログ-詳細")
        except Exception, inst:
            recdblist.addCommonlogEX(u"Error",u"tv2b25ts(tv2ts.py)", str(type(inst)),str(inst)+traceback.format_exc())
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(-1)
        else:
            status.changeTERecording(-1)
    elif recording_earth_pt1.useDVB()==1:
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(1)
        else:
            status.changeTERecording(1)
        try:
            recording_earth_pt1.record(ch,tsid,pout,time,0)
        except Exception, inst:
            recdblist.addCommonlogEX(u"Error",u"tv2b25ts(tv2ts.py)", str(type(inst)),str(inst)+traceback.format_exc())
        if len(ch) > 2:#BS/CSは100とかCS??とかなので3文字以上
            status.changeBSCSRecording(-1)
        else:
            status.changeTERecording(-1)
    if os.path.exists(pout):
        try:
            os.chmod(pout,0755)
        except:
            ""
            
def b252tsmix(pin, pout):
    if not (os.path.exists(pout) and os.path.getsize(pin)>os.path.getsize(pout)*0.95 and os.path.getsize(pin)<os.path.getsize(pout)*1.05):
        exe=""
        if configreader.getConfPath('b25_remote')=="1":
            try:
                exe = configreader.getConfPath('b25_env')+" && "
            except:
                inst=u"b25_remoteがオンになっていますが、b25_envが設定されていないかコメントアウトされています。"
                recdblist.addCommonlogEX(u"Error",u"b252tsmix(tv2ts.py)", "",inst)
        exe = exe + "nice -n 17 " + configreader.getConfPath('b25')
        doexe = exe + u' \"' + pin + u'\" \"' + pout + u'\"'
        recdblist.printutf8(doexe)
        recdblist.addCommandSelfLog(pin,doexe)
        txt = unicode(commands.getoutput(doexe.encode('utf-8')),'utf-8')
        recdblist.addLog(pin, doexe, u"b25ログ-コマンド")
        recdblist.addLog(pin, txt, u"b25ログ-詳細")
        recdblist.addCommandLogZip(pin, "b25", "b25", doexe, txt)
        return txt
    
def tsmix2ts(pin, pout, csch):#csch=0ならcsの処理をしない
    tssplitterex=configreader.getConfPath("java")+" -jar "+configreader.getConfPath("jTsSplitter")
    doexe =tssplitterex + " \""+ pin + "\" \""+ pout + "\" " + str(csch)
    doexe = "nice -n 18 " + doexe
    os.environ['LANG']="ja_JP.UTF-8"
    txt = unicode(commands.getoutput(doexe.encode('utf-8')),'utf-8')
    recdblist.addLog(pin, doexe, u"TsSplitログ-コマンド")
    recdblist.addLog(pin, txt, u"TsSplitログ-詳細")
    time.sleep(1)
    if os.access(pout, os.F_OK) and os.path.getsize(pout)>10*1000*1000:
        os.remove(pin)
        
