#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2014 Yukikaze

z_ascii = u"ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ　！”＃＄％＆’（）＊＋，−．／：；＜＝＞？＠［¥］＾＿‘｛｜｝〜　〜"
h_ascii = u"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz !\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ -"
z_ascii_sp = unichr(0x2212)+unichr(0xff0e)
h_ascii_sp = unichr(0x002d)+unichr(0x002e)
z_number = u"０１２３４５６７８９"
h_number = u"0123456789"
z_alphabet = u"ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ　＃"
h_alphabet = u"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz #"

def toHankaku(str):
    retstr = u""
    for s in str:
        i = z_ascii.find(s)
        if (i != -1):
            s = h_ascii[i]
        i = z_ascii_sp.find(s)
        if (i != -1):
            s = h_ascii_sp[i]
        i = z_number.find(s)
        if (i != -1):
            s = h_number[i]
        retstr = retstr + s
    return retstr

def toHankaku_ABC123(str):
    retstr = u""
    for s in str:
        i = z_alphabet.find(s)
        if (i != -1):
            s = h_alphabet[i]
        i = z_ascii_sp.find(s)
        if (i != -1):
            s = h_ascii_sp[i]
        i = z_number.find(s)
        if (i != -1):
            s = h_number[i]
        retstr = retstr + s
    return retstr

def checkCharacterType(character):
    """
    return code is 1:Alphabet 2:Hiragana 3:Katakana 4:Kanji
    """
    chcode=ord(character)
    if chcode>=0x0000 and chcode<=0x007F:
        return 1
    elif chcode>=0x3040 and chcode<=0x309F:
        return 2
    elif chcode>=0x30A0 and chcode<=0x30FF:
        return 3
    elif chcode>=0x4E00 and chcode<=0x9FFF:
        return 4
