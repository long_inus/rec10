#!/usr/bin/python
# coding: UTF-8
# Rec10 TS Recording Tools
# Copyright (C) 2009-2011 Yukikaze

import os
import os.path
import zipfile

def addFile2FileZip(addfile,basefile):
    th=0

    if os.path.exists(basefile):
        th=zipfile.ZipFile(basefile,'a',zipfile.ZIP_DEFLATED)
    else:
        th=zipfile.ZipFile(basefile,'w',zipfile.ZIP_DEFLATED)
    if os.path.exists(addfile):
        th.write(addfile,os.path.basename(addfile))
    th.close()
