#ifndef UTIL_H
#define UTIL_H 1

#define MAXSECLEN 4096

#include <time.h>
//#define BCD(n) ((n/100)<<8)|(((n%100)/10)<<4)|(n%10)
#define BCD(n) ((n>>4)*10+(n&0xf))

#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */

	int strrep(char *buf, char *mae, char *ato);
	int    getBit(unsigned char *byte, int *pbit, int gbit);
	void   getStr(char *tostr, unsigned char *byte, int *pbit, int len);
	int    parseOTHERdesc(unsigned char *data);
	void*  allocCopy(void* src, int *boff, size_t size);
	time_t parseMJD( unsigned char *data );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
